package ru.papyrus.collector.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import ru.papyrus.collector.entity.Source;
import ru.papyrus.collector.main.HibernateUtil;
import ru.papyrus.collector.main.logger.DBLogger;

/**
 *
 * @author roux
 */
public class SourceDAOImpl implements SourceDAO{
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private DBLogger DB_LOGGER;

    @Autowired
    private SessionDAO sessionDAO;
    
    @Override
    public boolean updateSource(Source source) {
//        DB_LOGGER.debug("Это updateSource, source: " + source);
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(source);
            session.getTransaction().commit();
            return true;
        } catch (Exception e) {
            DB_LOGGER.error("Ошибка при обновлении источника. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                DB_LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return false;
    }

    @Override
    public boolean addSource(Source source) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(source);
            session.getTransaction().commit();
            DB_LOGGER.info("Добавлен новый источник: " + source.getName());
            return true;
        } catch (Exception e) {
            DB_LOGGER.error("Ошибка при добавлении источника. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                DB_LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return false;
    }

    @Override
    public Collection<Source> getAllSources() {
        List<Source> r = new ArrayList<>();
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Source.class);
            r = (List<Source>)criteria.list();
            session.close();
        } catch (Exception e) {
            DB_LOGGER.error("Ошибка при получении всех источников. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                DB_LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return r;
    }

    /**
     * Получаем активные источники
     * @return 
     */
    @Override
    public Collection<Source> getActiveSources() {
        List<Source> r = new ArrayList<>();
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Source.class);
            criteria.add(Restrictions.eq("active", true));
            r = (List<Source>)criteria.list();
            session.close();
        } catch (Exception e) {
            DB_LOGGER.error("Ошибка при получении активных источников. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                DB_LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return r;
    }

    @Override
    public Collection<Source> getCommonSources() {
        List<Source> r = new ArrayList<>();
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Source.class);
            criteria.add(Restrictions.eq("common", true));
            r = (List<Source>)criteria.list();
            session.close();
        } catch (Exception e) {
            DB_LOGGER.error("Ошибка при получении общих источников. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                DB_LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return r;
    }

    /**
     * Выбираем из БД источники по заданному списку имён.
     * @param names
     * @return 
     */
    @Override
    public JSONArray retrieveSourcesByName(JSONArray names) {
//        DB_LOGGER.debug("Это retrieveSourcesByName, names: " + names.length());
//        for (int i = 0; i < names.length(); i++) {
//            DB_LOGGER.debug("retrieveSourcesByName name: " + names.get(i));
//        }
        List<String> valuesList = new ArrayList<>();
        for (int i = 0; i < names.length(); i++) {
//            DB_LOGGER.debug("name: " + names.getString(i));
            valuesList.add(names.getString(i));
        }
        
        JSONArray r = new JSONArray();
        if(valuesList.size() == 0) {
            DB_LOGGER.error("Пустой список имён.");
            return r;
        }
        
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Source.class);
            criteria.add(Restrictions.in("name", valuesList));
            List<Source> result = (ArrayList<Source>)criteria.list();
//            LOGGER.debug("result: " + result.size());
            for (Source source: result) {
//                LOGGER.debug("source: " + source.getName());
                JSONObject sourceObj = new JSONObject();
                sourceObj.put("name", source.getName());
                sourceObj.put("sourceClass", source.getSourceClass());
                sourceObj.put("tracing", source.getTracing());
                sourceObj.put("startPage", source.getStartPage());
                sourceObj.put("description", source.getDescription());
                sourceObj.put("active", source.getActive());
                sourceObj.put("common", source.getCommon());
                sourceObj.put("interval", source.getInterval());
                sourceObj.put("encoding", source.getEncoding());
                sourceObj.put("newsPageLink", source.getNewsPageLink());
                sourceObj.put("newsLink", source.getNewsLink());
                sourceObj.put("newsTitle", source.getNewsTitle());
                sourceObj.put("newsContent", source.getNewsContent());
                sourceObj.put("newsDate", source.getNewsDate());
                sourceObj.put("newsDateFormat", source.getNewsDateFormat());
                sourceObj.put("id", source.getId());

                r.put(sourceObj);

//                LOGGER.debug("sourceObj: " + sourceObj.toString());
            }

//            LOGGER.debug("r: " + r.toString());
            session.close();
        } catch(Exception e) {
            DB_LOGGER.error("Ошибка при получении JSON источников по указанным именам " + e.getClass().getSimpleName() + ": " + e.getMessage());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
//        DB_LOGGER.debug("Возвращается " + r.toString(4));
        return r;
    }

    /**
     * Получаем из БД имена всех имеющихся источников.
     * @return 
     */
    @Override
    public JSONArray retrieveSourceNames(String sessionId) {
//        DB_LOGGER.debug("Это getSourceNames");

        JSONArray r = new JSONArray();
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Source.class);
//            if (userSession == null) {
//                /* Если сессии нет, то только общедоступные */
//                criteria.add(Restrictions.eq("common", true));
//            } else {
//                /* Если сессия есть, то общедоступные плюс из подписок */
//                List<Source> subscriptionSources = userSession.getUser().getSubscriptionSources();
//                criteria.add(Restrictions.or(Restrictions.eq("common", true), Restrictions.in("source", subscriptionSources)));
//            }

            List<Source> result = (List<Source>)criteria.list();
            for (Source source : result) {
                r.put(source.getName());
            }
            session.close();
        } catch (Exception e) {
            DB_LOGGER.error("Ошибка при получении имён источников. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                DB_LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return r;
    }

    @Override
    public Collection<Source> getSourcesByNames(JSONArray sourcesJSONArray) {
        String[] names = new String[sourcesJSONArray.length()];
        for (int i = 0; i < sourcesJSONArray.length(); i++) {
            names[i] = (String)sourcesJSONArray.get(i);
        }
        return getSourcesByNames(names);
    }

    @Override
    public Source getSourceByName(String name) {
        Session session = null;
        Source ret = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Source.class);
            criteria.add(Restrictions.eq("name", name));
            ret = (Source)criteria.uniqueResult();
        } catch (Exception e) {
            DB_LOGGER.error("Ошибка при получении источника по имени. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                DB_LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return ret;
    }

    @Override
    public Collection<Source> getSourcesByNames(String... names) {
//        LOGGER.debug("Это getSourcesByNames, names: " + names.length);
//        for (int i = 0; i < names.length; i++) {
//            LOGGER.debug("name: " + names[i]);
//        }

        List<Source> ret = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Source.class);
            if (names != null) {
                criteria.add(Restrictions.in("name", names));
            }
            ret = (List<Source>)criteria.list();
        } catch (Exception e) {
            DB_LOGGER.error("Ошибка при получении источников по именам. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                DB_LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return ret;
    }

    @Override
    public Source getSourceById(long id) {
//        LOGGER.debug("Это getSourceById, id: " + id);
        Session session = null;
        Source ret = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Source.class);
            criteria.add(Restrictions.eq("id", id));
            ret = (Source)criteria.uniqueResult();
        } catch (Exception e) {
            DB_LOGGER.error("Ошибка при получении источника по его id. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                DB_LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return ret;
    }

    /**
     * Удаляем источники по заданному списку
     * @param source
     * @return
     */
    @Override
    public boolean deleteSource(Source source) {
//        LOGGER.debug("Это deleteSources, sources: " + sources.size());
        boolean ret = false;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            session.delete(source);

            session.getTransaction().commit();
            ret = true;
        } catch (Exception e) {
            session.getTransaction().rollback();
            LOGGER.error("Ошибка при удалении источника. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return ret;
    }
}