package ru.papyrus.collector.dao;

import ru.papyrus.collector.entity.User;

public interface UsersDAO {
    boolean addUser(User user);

    User getUserByLogin(String login);

    boolean modifyUser(User user);
}
