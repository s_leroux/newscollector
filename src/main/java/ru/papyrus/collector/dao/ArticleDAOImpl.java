package ru.papyrus.collector.dao;

import java.util.*;
import java.util.stream.Collectors;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import ru.papyrus.collector.entity.Article;
import ru.papyrus.collector.entity.Source;
import ru.papyrus.collector.entity.UserSession;
import ru.papyrus.collector.main.HibernateUtil;

import javax.servlet.http.HttpSession;

/**
 *
 * @author roux
 */
public class ArticleDAOImpl implements ArticleDAO{
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    
//    @Autowired
//    private DBLogger DB_LOGGER;

    @Autowired
    private SessionDAO sessionDAO;

    @Autowired
    private SourceDAO sourceDAO;

    @Override
    public Collection<Article> getAllArticles() {
//        DB_LOGGER.debug("Это getAllArticles");
        List<Article> r = new ArrayList<>();
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Article.class);
            r = (List<Article>)criteria.list();
            session.close();
        } catch (Exception e) {
            LOGGER.error("Ошибка при получении всех статей. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return r;
    }

    @Override
    public Collection<Article> getArticleByLink(String link) {
//        DB_LOGGER.debug("Это getArticleByLink, link: " + link);
        List<Article> r = new ArrayList<>();
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Article.class);
            criteria.add(Restrictions.eq("newsLink", link));
            r = (List<Article>)criteria.list();
        } catch (HibernateException e) {
            LOGGER.error("Ошибка при получении статей по ссылке. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return r;
    }

    @Override
    public boolean addArticle(Article article) {
//        DB_LOGGER.debug("Это addArticle: " + article.getNewsTitle());
        Session session = null;
        boolean success = false;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(article);
            session.getTransaction().commit();
            success = true;
        } catch (Exception e) {
            LOGGER.error("Ошибка при добавлении статьи. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return success;
    }

    /**
     * Получаем количество статей, лежащих по заданному линку.
     * @param link
     * @return 
     */
    @Override
    public Long getArticlesCountByLink(String link) {
        Session session = null;
        Long r = 0l;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Article.class);
            criteria.add(Restrictions.eq("newsLink", link));
            criteria.setProjection(Projections.rowCount());
            r = (Long)criteria.uniqueResult();
            session.close();
        } catch (HibernateException e) {
            LOGGER.error("Ошибка при получении статей по ссылке. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return r;
    }

    /**
     * Получаем список уже существующих в БД ссылок по заданному имени источника.
     * Это нужно для решения вопроса, скачивать или нет очередную статью.
     * @param sourceName
     * @return 
     */
    @Override
    public List<String> getLinksBySourceName(String sourceName) {
//        LOGGER.debug("Это getLinksBySourceName, sourceName: " + sourceName);
        List<String> r = new ArrayList<>();
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query q = session.createQuery("select newsLink from Article a where a.source.name = :name ");
            q.setParameter("name", sourceName);
            r = (List<String>)q.list();
            session.close();
        } catch (HibernateException e) {
            LOGGER.error("Ошибка при получении ссылок по имени источника. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return r;
    }

    /**
     * Получаем из БД статьи по заданными параметрам
     * @param parameterMap
     */
    @Override
    public JSONObject retrieveArticles(Map<String, String[]> parameterMap, HttpSession httpSession) {
//        LOGGER.debug("Это retrieveArticles");

        JSONObject ret = new JSONObject();
        JSONArray articlesJSON = new JSONArray();
        Integer articlesCount = null;
        try {
            articlesCount = Integer.parseInt(parameterMap.get("articlesCount")[0]);
        } catch (Exception e) {
        }

        Session session = null;

        UserSession userSession = sessionDAO.getSessionById(httpSession.getId());
//        LOGGER.debug("userSession: " + userSession);

        try {
            session = HibernateUtil.getSessionFactory().openSession();

            Criteria criteria = session.createCriteria(Article.class);

            if(articlesCount != null) {
                /* Это вытаскивание N последних статей */
//                LOGGER.debug("Это вытаскивание N последних статей");
//                LOGGER.debug("articlesCount: " + articlesCount);
                criteria.setMaxResults(articlesCount);
                ret.put("count", articlesCount);

                List<String> commonSourceNames = sourceDAO.getCommonSources().stream().map(Source::getName).collect(Collectors.toList());
//                LOGGER.debug("commonSourceNames: " + commonSourceNames.size());

                if (userSession == null) {
                    /* Если нет сессии, то статьи только из общедоступных источников */
//                    LOGGER.debug("Если нет сессии, то статьи только из общедоступных источников");
//                    criteria.createAlias("source", "s").add(Restrictions.eq("s.common", true));
                    criteria.add(Restrictions.in("sourceName", commonSourceNames));
                } else {
                    /* Если сессия есть, то статьи только из источников по подписке */
//                    LOGGER.debug("Если сессия есть, то статьи только из источников по подписке");
                    List<String> subscriptionSourceNames = userSession.getUser().getAccessSources(true).stream().map(Source::getName).collect(Collectors.toList());
//                    LOGGER.debug("subscriptionSourceNames: " + subscriptionSourceNames);

                    if (subscriptionSourceNames.isEmpty()) {
                        criteria = null;
                    } else {
                        criteria.add(Restrictions.in("sourceName", subscriptionSourceNames));
                    }
                }
            } else {
                /* А это вытаскивание статей по параметрам поиска */
//                LOGGER.debug("А это вытаскивание статей по параметрам поиска");
                /* По источникам */
                String[] sourceNames = parameterMap.get("sources[]");

                if (sourceNames != null && sourceNames.length > 0) {
                    criteria.add(Restrictions.in("sourceName", sourceNames));
                }

                /* По дате начала */
                try {
                    Date dateFrom = new Date(Long.parseLong(parameterMap.get("dateFrom")[0]));
                    criteria.add(Restrictions.ge("articleDate", dateFrom));
                } catch (Exception e) {
                    /* Не добавляем никакого ограничения */
                }

                /* По дате окончания */
                try {
                    Date dateTo = new Date(Long.parseLong(parameterMap.get("dateTo")[0]));
                    criteria.add(Restrictions.le("articleDate", dateTo));
                } catch (Exception e) {
                    /* Не добавляем никакого ограничения */
                }

                /* По контексту заголовка */
                String titleContext = parameterMap.get("titleContext")[0];
                if (titleContext != null && !titleContext.trim().isEmpty()) {
                    criteria.add(Restrictions.ilike("newsTitle", titleContext, MatchMode.ANYWHERE));
                }
                /* По контексту тела статьи */
                String bodyContext = parameterMap.get("bodyContext")[0];
                if (bodyContext != null && !bodyContext.trim().isEmpty()) {
                    criteria.add(Restrictions.ilike("content", bodyContext, MatchMode.ANYWHERE));
                }

                criteria.setProjection(Projections.rowCount());
                ret.put("count", criteria.uniqueResult());
                criteria.setProjection(null);

                /* Не более 1000 результатов */
                criteria.setMaxResults(1000);
            }

            List<Article> articles = null;
            if (criteria != null) {
                criteria.addOrder(Order.desc("articleDate"));
                try {
                    articles = criteria.list();
                } catch (Exception e) {
                    LOGGER.error("Exception при получении списка статей " + e.getClass().getSimpleName() + ": " + e.getMessage());
                }
            }

//            LOGGER.debug("articles: " + articles);
//            LOGGER.debug("Получено статей из базы: " + articles.size());

            if (articles != null) {
                try {
                    for (Article article : articles) {
                        articlesJSON.put(article.toJSON());
                    }
                } catch (Exception e) {
                    LOGGER.error("Exception при итерации статей: " + e.getClass().getSimpleName() + ": " + e.getMessage());
                    for (int i = 0; i < e.getStackTrace().length; i++) {
                        LOGGER.error(""+e.getStackTrace()[i]);
                    }
                }
            }

            ret.put("articles", articlesJSON);

            return ret;
        } catch (Exception e) {
            LOGGER.error("Ошибка при получении статей из БД " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                LOGGER.error(""+e.getStackTrace()[i]);
            }
            return null;
        } finally {
            if(session != null && session.isOpen()) {
                session.disconnect();
                session.close();
            }
        }
    }
}