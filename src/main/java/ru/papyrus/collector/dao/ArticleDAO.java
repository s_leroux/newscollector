package ru.papyrus.collector.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import ru.papyrus.collector.entity.Article;

import javax.servlet.http.HttpSession;

/**
 *
 * @author roux
 */
public interface ArticleDAO {
    public Collection<Article> getAllArticles();
    
    public Collection<Article> getArticleByLink(String link);
    
    public Long getArticlesCountByLink(String link);
    
    public boolean addArticle(Article article);
    
    public List<String> getLinksBySourceName(String sourceName);

    public JSONObject retrieveArticles(Map<String, String[]> parameterMap, HttpSession httpSession);
}
