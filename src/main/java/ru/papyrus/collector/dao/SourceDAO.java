package ru.papyrus.collector.dao;

import org.json.JSONArray;
import ru.papyrus.collector.entity.Source;

import java.util.Collection;

/**
 * Интерфейс доступа к таблице SOURCES
 * @author roux
 */
public interface SourceDAO {
    boolean addSource(Source source);
    
    boolean updateSource(Source source);
    
//    public void editSource(Source source);
    
//    public void deleteSourceById(Integer id);
    
    Collection<Source> getAllSources();
    
    Collection<Source> getActiveSources();

    Collection<Source> getCommonSources();
    
//    public Collection<Source> getSourcesById(Collection<Integer> ids);
    
    JSONArray retrieveSourcesByName(JSONArray names);
    
    JSONArray retrieveSourceNames(String sessionId);

    Collection<Source> getSourcesByNames(String... names);
    Collection<Source> getSourcesByNames(JSONArray sourcesJSONArray);

    Source getSourceByName(String name);
    Source getSourceById(long id);

    boolean deleteSource(Source source);
}
