/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.papyrus.collector.dao;

import org.json.JSONArray;
import ru.papyrus.collector.entity.LogRecord;

import java.util.Date;

/**
 *
 * @author roux
 */
public interface LogDAO {
//    public void addLogRecord(Date recordDate, LogLevels logLevel, String sourceName, String text);
    public boolean addLogRecord(LogRecord record);
    
    public void deleteObsoleteLogs();

    public JSONArray getLogByDate(Date date);
}
