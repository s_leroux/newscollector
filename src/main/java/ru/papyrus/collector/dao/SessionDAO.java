package ru.papyrus.collector.dao;

import ru.papyrus.collector.entity.UserSession;

public interface SessionDAO {
    boolean addSession(UserSession userSession);

    boolean deleteSessionById(String sessionId);

    void deleteExpiredSessions();

    UserSession getSessionById(String sessionId);
}
