/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.papyrus.collector.dao;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import ru.papyrus.collector.entity.LogRecord;
import ru.papyrus.collector.main.HibernateUtil;

/**
 *
 * @author roux
 */
public class LogDAOImpl implements LogDAO{
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    
//    private final SimpleDateFormat displayDateFormat = new SimpleDateFormat("dd.MM.YYYY HH:mm:ss");

    /**
     * Конструктор без параметров
     */
    public LogDAOImpl() {
//        LOGGER.debug("Это Конструктор LogDAOImpl без параметров");
    }

    @Override
    public boolean addLogRecord(LogRecord record) {
        Session session = null;
        boolean success = false;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(record);
            session.getTransaction().commit();
            success = true;
        } catch (Exception e) {
            LOGGER.error("Ошибка при добавлении собщения в лог. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return success;
    }

    /**
     * Удаляются все записи лога, сделанные больше месяца назад.
     */
    @Override
    public void deleteObsoleteLogs() {
//        LOGGER.debug("Надо удалить все старые логи. Старые - это, скажем, более месяца назад.");
        Session session = null;
        try {
            Calendar c = Calendar.getInstance();
//            c.add(Calendar.DAY_OF_YEAR, -1);
            c.add(Calendar.MONTH, -1);
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery("delete from LogRecord lr where lr.recordDate < :date");
            q.setParameter("date", c.getTime());
            q.executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            LOGGER.error("Ошибка при удалении лога. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    /**
     * Получаем содержимое лога за указанную дату
     * @return 
     */
    @Override
    public JSONArray getLogByDate(Date date) {
//        LOGGER.debug("Это getLogByDate, date: " + date);
        
        JSONArray ret = new JSONArray();
        
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Criteria criteria = session.createCriteria(LogRecord.class);

            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.DAY_OF_YEAR, 1);

            criteria.add(Restrictions.ge("recordDate", date));
            criteria.add(Restrictions.lt("recordDate", c.getTime()));
            List<LogRecord> res = criteria.list();
            
            /* Упорядочить по времени в обратном порядке и передать */
            Set<LogRecord> logRecords = new TreeSet<>(new Comparator<LogRecord>(){
                @Override
                public int compare(LogRecord o1, LogRecord o2) {
                    return o2.getRecordDate().compareTo(o1.getRecordDate());
                }
            });
            for (LogRecord record : res) {logRecords.add(record);}
//            for (LogRecord logRecord : logRecords) {
//                LOGGER.debug("logRecord: " + logRecord.getRecordDate() + ", text: " + logRecord.getText());
//            }

            for (LogRecord logRecord : logRecords) {
                ret.put(logRecord.toJSON());
            }
        } catch (Exception e) {
            LOGGER.error("Ошибка при получении сегодняшнего лога " + e.getClass().getSimpleName() + ": " + e.getMessage());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return ret;
    }
}
