package ru.papyrus.collector.dao;

        import org.hibernate.Criteria;
        import org.hibernate.Session;
        import org.hibernate.criterion.Restrictions;
        import org.slf4j.Logger;
        import org.slf4j.LoggerFactory;
        import org.springframework.beans.factory.annotation.Autowired;
        import ru.papyrus.collector.entity.User;
        import ru.papyrus.collector.main.HibernateUtil;
        import ru.papyrus.collector.main.logger.DBLogger;

public class UserDAOImpl implements UsersDAO {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DBLogger DB_LOGGER;

    @Override
    public boolean addUser(User user) {
//        LOGGER.debug("Это addUser");
        user.setLogin(user.getLogin().toLowerCase());
        Session session = null;
        boolean res = false;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
            res = true;
//            LOGGER.debug("Успешно добавлен пользователь " + user.toString() + ".");
            DB_LOGGER.info("Успешно добавлен пользователь " + user.toString() + ".");
        } catch (Exception e) {
//            LOGGER.error("Ошибка при добавлении пользователя. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            DB_LOGGER.error("Ошибка при добавлении пользователя. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                DB_LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return res;
    }

    @Override
    public User getUserByLogin(String login) {
        Session session = null;
        User res = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(User.class);
            criteria.add(Restrictions.eq("login", login).ignoreCase());
            res = (User)criteria.uniqueResult();
        } catch (Exception e) {
            LOGGER.error("Ошибка при получении пользователя по логину. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return res;
    }

    /**
     * Изменяем параметры ссуществующего пользователя
     * @param user
     * @return
     */
    @Override
    public boolean modifyUser(User user) {
//        LOGGER.debug("Это modifyUser, user login: " + user.getLogin() + " доступов: " + user.getAccesses().size());
        Session session = null;
        boolean ret = false;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.saveOrUpdate(user);
            session.getTransaction().commit();
            ret = true;
        } catch (Exception e) {
            LOGGER.error("Ошибка при изменении параметров пользователя. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return ret;
    }
}