package ru.papyrus.collector.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ru.papyrus.collector.entity.UserSession;
import ru.papyrus.collector.main.HibernateUtil;
import ru.papyrus.collector.main.logger.DBLogger;

import java.util.Calendar;

public class SessionDAOImpl implements SessionDAO{
    private static final Logger LOGGER = LoggerFactory.getLogger(SessionDAOImpl.class);
    @Autowired
    private DBLogger DB_LOGGER;

    @Override
    public boolean addSession(UserSession userSession) {
        Session session = null;
        boolean success = false;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            session.save(userSession);
            session.getTransaction().commit();
            success = true;
        } catch (ConstraintViolationException e) {
//            DB_LOGGER.warn("Это ConstraintViolationException. Видимо, сессия с таким id уже есть в БД.");
            if (session != null && session.isOpen()) {
                session.getTransaction().rollback();
            }
        } catch (Exception e) {
            DB_LOGGER.error("Ошибка при добавлении сессии. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                DB_LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return success;
    }

    /**
     * Удаляем сессию по заданному id
     * @param sessionId
     * @return
     */
    @Override
    public boolean deleteSessionById(String sessionId) {
        Session session = null;
        boolean success = false;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery("delete from UserSession s where s.sessionId = :id");
            q.setParameter("id", sessionId);
            int affected = q.executeUpdate();
            session.getTransaction().commit();
            success = true;
        } catch (Exception e) {
            LOGGER.error("Ошибка при удалении сессии по id, " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return success;
    }

    /**
     * Удаляем старые сессии. Пока будем считать, что старые - это те, которые были созданы более суток назад.
     */
    @Override
    public void deleteExpiredSessions() {
        Session session = null;
        try {
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DAY_OF_MONTH, -1);
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery("delete from UserSession s where s.creationTime < :date");
            q.setParameter("date", c.getTime());
            int affected = q.executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            LOGGER.error("Ошибка при удалении старой сессии, " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public UserSession getSessionById(String sessionId) {
        Session session = null;
        UserSession res = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(UserSession.class);
            criteria.add(Restrictions.eq("sessionId", sessionId));
            res = (UserSession)criteria.uniqueResult();
        } catch (Exception e) {
            LOGGER.error("Ошибка при получении сессии по её id. " + e.getClass().getSimpleName() + ": " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length; i++) {
                LOGGER.error(""+e.getStackTrace()[i]);
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return res;
    }
}
