/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.papyrus.collector.entity;

/**
 *
 * @author roux
 */
public enum LogLevels {
    TRACE(0),
    DEBUG(1),
    INFO(2),
    WARN(3),
    ERROR(4),
    FATAL(5);

    private int level;

    LogLevels(int imp) {
        this.level = imp;
    }

    public int getLevel() {
        return level;
    }
}
