package ru.papyrus.collector.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Entity для сессии
 */
@Entity
@Table(name="SESSIONS")
public class UserSession {

    /**
     * Конструктор по умолчанию
     */
    public UserSession() {

    }

    /**
     * Конструктор по id сессии и id пользователя. Пока только эти параметры.
     * @param sessionId
     * @param user
     */
    public UserSession(String sessionId, User user, Date creationTime) {
        this.sessionId = sessionId;
        this.user = user;
        this.creationTime = creationTime;
    }

    @Id
    @Column(name="ID", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "SESSION_ID", nullable = false, length = 32, unique = true)
    private String sessionId;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "USER_ID")
    private User user;

    @Column(name="CREATED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationTime;

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public User getUser() {
        return user;
    }

    public void setUserId(User user) {
        this.user = user;
    }
}