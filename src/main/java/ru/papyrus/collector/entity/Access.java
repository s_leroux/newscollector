package ru.papyrus.collector.entity;

import javax.persistence.*;

/**
 * Класс-сущность, отображающий доступ пользователя к источнику (в том числе наличие подписки)
 * Предполагается, что доступ к общедоступным источникам есть у всех - в таком случае Access не создаётся,
 * а создаётся он при доступе к общедоступному источнику только тогда, когда на этот источник оформляется подписка.
 * Для вновь создаваемого пользователя подписки оформляются на все общедоступные источники.
 */
@Entity
@Table(name="ACCESS")
public class Access {
    /**
     * Дефолтный конструктор
     */
    public Access() {

    }

    /**
     * Конструктор с указанием исочника, пользователя и наличия подписки
     * @param source
     * @param user
     * @param subscription
     */
    public Access(Source source, User user, Boolean subscription) {
        this.source = source;
        this.user = user;
        this.subscription = subscription;
    }


    @Id
    @Column(name = "ID", unique = true, nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "source_id", nullable = false, referencedColumnName = "id")
    private Source source;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false, referencedColumnName = "id")
    private User user;

    public Boolean getSubscription() {
        return subscription;
    }

    public void setSubscription(Boolean subscription) {
        this.subscription = subscription;
    }

    @Column(name = "SUBSCRIPTION", nullable = false)
    private Boolean subscription;
}
