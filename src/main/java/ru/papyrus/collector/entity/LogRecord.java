package ru.papyrus.collector.entity;

import org.json.JSONObject;
import ru.papyrus.collector.main.App;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * Класс представляет собой POJO для репрезентации одной записи в журнале
 * @author roux
 */
@Entity
@Table(name = "LOG", uniqueConstraints = {@UniqueConstraint(columnNames = "ID")})
public class LogRecord implements Serializable {

    public LogRecord() {
    }

    public LogRecord(Date recordDate, LogLevels logLevel, String sourceName, String text) {
        this.recordDate = recordDate;
        this.logLevel = logLevel;
        this.sourceName = sourceName;
        this.text = text;
    }
    
    @Id
    @Column(name = "ID", unique = true, nullable = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Column(name = "TIMEDATE")
    @Temporal(TemporalType.TIMESTAMP) 
    private Date recordDate;
    
    @Column(name="LOG_LEVEL")
    @Enumerated(EnumType.ORDINAL)
    private LogLevels logLevel;
    
    @Column(name="SOURCE", unique = false, nullable = true, length = 256)
    private String sourceName;
    
    @Column(name="TEXT", columnDefinition="TEXT")
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public LogLevels getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(LogLevels logLevel) {
        this.logLevel = logLevel;
    }

    public Date getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(Date recordDate) {
        this.recordDate = recordDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public JSONObject toJSON() {
        JSONObject json = new JSONObject();
        json.put("level", this.getLogLevel().name().toLowerCase());
        json.put("source", this.getSourceName());
        json.put("recordDate", App.DISPLAY_DATE_FORMAT.format(this.getRecordDate()));
        json.put("text", this.getText());

        return json;
    }
}
