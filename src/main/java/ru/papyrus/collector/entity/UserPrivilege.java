package ru.papyrus.collector.entity;

import ru.papyrus.collector.main.utils.Privileges;

import javax.persistence.*;

/**
 * Класс Entity для представления какой-то конкретной привилегии пользователя
 */
@Entity
@Table(name="USER_PRIVILEGES")
public class UserPrivilege {

    /**
     * Дефолтный конструктор
     */
    public UserPrivilege() {

    }

    /**
     * Конструктор
     * @param privilege
     */
    public UserPrivilege(User user, Privileges privilege) {
        this.privilege = privilege;
        this.user = user;
    }

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "NAME", length = 16)
    private Privileges privilege;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Privileges getPrivilege() {
        return privilege;
    }

    public void setPrivilege(Privileges privilege) {
        this.privilege = privilege;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
