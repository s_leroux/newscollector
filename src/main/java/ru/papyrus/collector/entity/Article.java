package ru.papyrus.collector.entity;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.*;

import org.hibernate.annotations.Index;
import org.json.JSONObject;

/**
 *
 * @author roux
 */
@Entity
@Table(name = "ARTICLES", uniqueConstraints = {@UniqueConstraint(columnNames = "ID")})
@org.hibernate.annotations.Table(appliesTo = "ARTICLES", indexes = {
        @Index(name = "article_title_idx", columnNames = {"TITLE"})
})
public class Article {
//    private final SimpleDateFormat displayDateFormat = new SimpleDateFormat("dd.MM.YYYY HH:mm:ss");
    
    /***
     * Конструктор без параметров
     */
    public Article() {
        
    }

    /**
     * Конструктор с заголовком, содержимым и датой публикации
     * @param sourceName
     * @param newsLink
     * @param newsTitle
     * @param content
     * @param articleDate
     */
    public Article(String sourceName, String newsLink, String newsTitle, String content, Date articleDate) {
        this.sourceName = sourceName;
        this.newsLink = newsLink;
        this.newsTitle = newsTitle;
        this.content = content;
        this.articleDate = articleDate;
    }
    
    @Id
    @Column(name = "ID", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

//    public Source getSource() {
//        return source;
//    }

    @Column(name = "SOURCE_NAME", nullable = true, length = 64)
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "SOURCE_ID", nullable = true)
    private String sourceName;
    
    /**
     * Заголовок новостной статьи
     */
    @Column(name = "TITLE", nullable = true, length = 256)
    private String newsTitle;
    
    /**
     * Содержимое новостной статьи
     */
    @Column(name = "CONTENT", columnDefinition="TEXT")
    private String content;
    
    /**
     * Дата публикации
     */
    @Column(name = "ARTICLE_DATE")
    @Temporal(TemporalType.TIMESTAMP) 
    private Date articleDate;
    
    @Column(name = "LINK", unique = true, nullable = false, length = 256)
    private String newsLink;
    
    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getNewsLink() {
        return newsLink;
    }

    public void setNewsLink(String newsLink) {
        this.newsLink = newsLink;
    }

    public Date getArticleDate() {
        return articleDate;
    }

    public void setArticleDate(Date articleDate) {
        this.articleDate = articleDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Article{" + "id=" + id + ", newsTitle=" + newsTitle + ", content=" + content + ", articleDate=" + articleDate + '}';
    }

    /**
     * Формируем JSON
     * @return 
     */
    public JSONObject toJSON() {
        JSONObject json = new JSONObject();

        if(this.getArticleDate() != null) {
            json.put("articleDate", new SimpleDateFormat("dd.MM.YYYY HH:mm:ss").format(this.getArticleDate()));
        } else {
            json.put("articleDate", "");
        }
        
        json.put("newsLink", this.getNewsLink());
        json.put("newsTitle", this.getNewsTitle());
        json.put("content", this.getContent());
        json.put("sourceName", this.sourceName);
        return json;
    }
}
