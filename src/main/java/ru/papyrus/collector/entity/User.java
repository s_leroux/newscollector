package ru.papyrus.collector.entity;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import ru.papyrus.collector.main.utils.Util;

import javax.persistence.*;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Класс Entity для представления пользователя.
 * То есть его, пользователя, основные характеристики.
 * Привилегии здесь не описываются.
 */
@Entity
@Table(name="USERS")
public class User {
    private static final Logger LOGGER = LoggerFactory.getLogger(User.class);

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;

    @Column(name="LOGIN", unique = true, nullable = false, length = 128)
    private String login;

    @Column(name="SALT", nullable = false, length = 128)
    private String salt;

    @Column(name="PASSWORD_HASH", nullable = false)
    private byte[] passwordHash;

    @Column(name="FAMILY_NAME", length = 256)
    private String familyName;

    @Column(name="NAME",length = 128)
    private String name;

    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<UserPrivilege> userPrivileges = new ArrayList<>(0);

    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Access> accesses = new ArrayList<>(0);

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }

    public String getSalt() {
        return salt;
    }
    public void setSalt(String salt) {
        this.salt = salt;
    }

    public byte[] getPasswordHash() {
        return passwordHash;
    }
    public void setPasswordHash(byte[] passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getFamilyName() {
        return familyName;
    }
    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public List<UserPrivilege> getUserPrivileges() {
        return userPrivileges;
    }
    public void setUserPrivileges(List<UserPrivilege> userPrivileges) {
        this.userPrivileges = userPrivileges;
    }

    public List<Access> getAccesses() {
        return accesses;
    }
    public void setAccesses(List<Access> accesses) {
        this.accesses = accesses;
    }


    /**
     * Конструктор без параметров
     */
    public User() {

    }

    /**
     * Конструктор с логином и паролем в виде строки
     * @param login
     * @param password
     * @throws NoSuchAlgorithmException
     */
    public User(String login, String password) {
        this(login, password, null);
    }

    /**
     * Конструктор и логином, паролем и фамилией
     * @param login
     * @param password
     * @param familyName
     * @throws NoSuchAlgorithmException
     */
    public User(String login, String password, String familyName) {
        this(login, password, familyName, null);
    }

    /**
     * Конструктор с логином, паролем, фамилией, именем и отчеством
     * @param login
     * @param password
     * @param familyName
     * @param name
     * @throws NoSuchAlgorithmException
     */
    public User(String login, String password, String familyName, String name){
        this.salt = UUID.randomUUID().toString();
        this.passwordHash = Util.getPwdHash(password, this.salt);
        this.login = login;
        this.familyName = familyName;
        this.name = name;
    }

    public void addPrivilege(UserPrivilege userPrivilege) {
//        LOGGER.debug("Это addPrivilege, userPrivilege: " + userPrivilege);
        this.userPrivileges.add(userPrivilege);
    }

    /**
     * Добавляем доступ
     * @param access
     */
    public void addAccess(Access access) {
        this.accesses.add(access);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if(StringUtils.hasText(this.familyName)) {
            sb.append(this.familyName);
        }
        if(StringUtils.hasText(this.name)) {
            if (StringUtils.hasText(sb)) sb.append(" ");
            sb.append(this.name);
        }

        sb.append(" (").append(this.login).append(")");
        return sb.toString();
    }

    public List<Source> getAccessSources(boolean subsOnly) {
        List<Source> ret = new ArrayList<>(0);
        for (Access access: this.accesses) {
            if (!subsOnly || access.getSubscription()) {
                ret.add(access.getSource());
            }
        }
        return ret;
    }

    /**
     * Удаляем все имеющиеся доступы
     */
    public void deleteAccesses() {
        this.accesses.clear();
    }

    public void setPassword(String pw) {
        this.passwordHash = Util.getPwdHash(pw, this.salt);
    }
}