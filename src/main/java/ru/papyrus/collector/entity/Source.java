package ru.papyrus.collector.entity;

import javax.persistence.*;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Класс представляет собой POJO для репрезентации описания источника информации
 * @author roux
 */
@Entity
@Table(name = "SOURCES", uniqueConstraints = {@UniqueConstraint(columnNames = "ID")})
public class Source {

    /**
     * Конструктор без параметров
     */
    public Source() {
    }
    
    /**
     * Конструктор по параметрам, переданным в виде JSON
     * @param obj 
     */
    public Source(JSONObject obj) {
//        if(includeId) {
//            this.setId(obj.getInt("id"));
//        }
        this.setName(obj.getString("name"));
        this.setActive(obj.getBoolean("active"));
        this.setStartPage(obj.getString("startPage"));
        this.setNewsContent(obj.getString("newsContent"));
        this.setDescription(obj.getString("description"));
        this.setEncoding(obj.getString("encoding"));
        this.setNewsLink(obj.getString("newsLink"));
        this.setNewsTitle(obj.getString("newsTitle"));
        this.setNewsDateFormat(obj.getString("newsDateFormat"));
        this.setInterval(obj.getString("interval"));
        this.setSourceClass(obj.getString("sourceClass"));
        this.setNewsPageLink(obj.getString("newsPageLink"));
        this.setNewsDate(obj.getString("newsDate"));
        this.setTracing(obj.getBoolean("tracing"));
        this.setCommon(obj.getBoolean("common"));
    }
    
//    /**
//     * Конструктор без путей
//     * @param name
//     * @param description
//     * @param interval 
//     */
//    public Source(String name, String description, String interval) {
//        
//    }

//    /**
//     * Конструктор без путей и описания
//     * @param name
//     * @param interval 
//     */
//    public Source(String name, String interval) {
//        this(name, null, interval);
//    }
    
    @Id
    @Column(name = "ID", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    public List<Access> getSourceAccesses() {
        return sourceAccesses;
    }

    public void setSourceAccesses(List<Access> sourceAccesses) {
        this.sourceAccesses = sourceAccesses;
    }

    @OneToMany(
            mappedBy = "source",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Access> sourceAccesses = new ArrayList<>(0);
    
    /**
     * Уникальное имя источника
     */
    @Column(name = "NAME", unique = true, nullable = false, length = 64)
    private String name;
    
    /**
     * Описание источника. Необязательный и малозначимый параметр
     */
    @Column(name = "DESCRIPTION", unique = false, nullable = true, length = 4096)
    private String description;
    
    /**
     * Флажок, означающий, будет ли этот источник обрабатываться.
     */
    @Column(name = "ACTIVE", nullable = false)
    private Boolean active = true;

    public Boolean getCommon() {
        return common;
    }

    public void setCommon(Boolean common) {
        this.common = common;
    }

    /**
     * Флажок, означающий, является ли этот источник общедоступным.
     */
    @Column(name = "COMMON", nullable = false)
    private Boolean common = true;
    
    /**
     * Интервал, через который будут посылаться запросы к источнику.
     */
    @Column(name = "FETCH_INTERVAL", nullable = false, length = 64)
    private String interval = "";
    
    /**
     * Класс java, который будет обрабатывать данный источник.
     */
    @Column(name = "SOURCE_CLASS", nullable = false, length = 256)
    private String sourceClass = "";
    
    /**
     * Кодировка на сайте источника. Применяется, если не удалось выяснить кодировку непосредственно на сайте.
     */
    @Column(name = "ENCODING", nullable = true, length = 32)
    private String encoding;
    
    /**
     * Страница, с которой начинается просмотр.
     */
    @Column(name = "START_PAGE", nullable = false, length = 512)
    private String startPage = "";
    
    /**
     * Ссылка на страницку нововстей. Может быть null - в этом случае нововсти будут искаться прямо не стартовой странице.
     * Записывается в виде xPath.
     */
    @Column(name = "NEWS_PAGE_LINK", nullable = true, length = 512)
    private String newsPageLink;
    
    /**
     * Ссылка на конкретную новость на странице с новостями.
     * Записывается в виде xPath.
     */
    @Column(name = "NEWS_LINK", nullable = true, length = 512)
    private String newsLink;
    
    /**
     * XPath для поиска заголовка новости.
     */
    @Column(name = "NEWS_TITLE", nullable = true, length = 256)
    private String newsTitle;

    /**
     * XPath для поиска текста новости
     */
    @Column(name = "NEWS_CONTENT", nullable = true, length = 256)
    private String newsContent;
    
    /**
     * XPath для поиска даты публикации новости на странице новости.
     */
    @Column(name = "NEWS_DATE", nullable = true, length = 256)
    private String newsDate;
    
    @Column(name = "NEWS_DATE_FORMAT", nullable = true, length = 64)
    private String newsDateFormat;
    
    @Column(name = "TRACING", nullable = true)
    private Boolean tracing;

    public Boolean getTracing() {
        return tracing;
    }

    public void setTracing(Boolean tracing) {
        this.tracing = tracing;
    }

    public String getNewsDateFormat() {
        return newsDateFormat;
    }

    public void setNewsDateFormat(String newsDateFormat) {
        this.newsDateFormat = newsDateFormat;
    }

    public String getNewsDate() {
        return newsDate;
    }

    public void setNewsDate(String newsDate) {
        this.newsDate = newsDate;
    }

    public String getNewsContent() {
        return newsContent;
    }

    public void setNewsContent(String newsContent) {
        this.newsContent = newsContent;
    }

    public String getNewsPageLink() {
        return newsPageLink;
    }

    public void setNewsPageLink(String newsPageLink) {
        this.newsPageLink = newsPageLink;
    }

    public String getNewsLink() {
        return newsLink;
    }

    public void setNewsLink(String newsLink) {
        this.newsLink = newsLink;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getSourceClass() {
        return sourceClass;
    }

    public void setSourceClass(String sourceClass) {
        this.sourceClass = sourceClass;
    }

    public String getStartPage() {
        return startPage;
    }

    public void setStartPage(String startPage) {
        this.startPage = startPage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Source source = (Source) o;
        return name.equals(source.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
