package ru.papyrus.collector.servlet;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.json.JSONObject;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.papyrus.collector.entity.Source;
import ru.papyrus.collector.entity.UserPrivilege;
import ru.papyrus.collector.entity.UserSession;
import ru.papyrus.collector.main.App;
import ru.papyrus.collector.main.utils.Privileges;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;

@WebServlet(value="/NewsCollector/getTabs")
public class TabsServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(TabsServlet.class);

    /**
     * Возвращаем закладки и их содержимое в соответствии с правами пользователя
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String sessionId = req.getSession().getId();
//        LOGGER.debug("Это TabsServlet, sessionId: " + sessionId);

        UserSession userSession = App.getInstance().getSessionDAO().getSessionById(sessionId);

//        LOGGER.debug("userSession: " + userSession);

        /* Возвращаемый JSON */
        JSONArray tabsArray = new JSONArray();

        if(userSession != null) {
//            LOGGER.debug("User: " + userSession.getUser());
//            LOGGER.debug("Subscriptions size: " + userSession.getUser().getSubscriptions().size());

            List<UserPrivilege> privileges = userSession.getUser().getUserPrivileges();
//            LOGGER.debug("UserPrivileges: " + privileges.size());
//            LOGGER.debug("Добавляем закладки");
            VelocityEngine engine = App.getInstance().getVelocityEngineFactory().getObject();

            for (UserPrivilege priv: privileges) {
                /* Закладка источников */
                if (Privileges.SOURCES.name().equals(priv.getPrivilege().name())) {
                    Template sourcesTemplate = engine.getTemplate("velocity/sources.vm", "UTF-8");
                    VelocityContext sourcesVelocityContext = new VelocityContext();
//                    List<String> sourcesList = new ArrayList<>();
//                    Collection<Source> sources = App.getInstance().getSourceDAO().getAllSources();
//                    for (Source source: sources) {
//                        sourcesList.add(source.getName());
//                    }
//                    sourcesVelocityContext.put("sourcesList", sourcesList);
                    StringWriter writer = new StringWriter();
                    sourcesTemplate.merge(sourcesVelocityContext, writer);
                    JSONObject sourceTab = new JSONObject();
                    sourceTab.put("name", "Источники");
                    sourceTab.put("content", writer.toString());
                    sourceTab.put("func", "renderSources");
                    sourceTab.put("script", "sources.js?v=" + System.currentTimeMillis());
                    tabsArray.put(sourceTab);
                }

                /* Закладка журнала */
                if (Privileges.LOG.name().equals(priv.getPrivilege().name())) {
                    Template logTemplate = engine.getTemplate("velocity/log.vm", "UTF-8");
                    StringWriter writer = new StringWriter();
                    logTemplate.merge(new VelocityContext(), writer);
                    JSONObject logTab = new JSONObject();
                    logTab.put("name", "Журнал");
                    logTab.put("content", writer.toString());
                    logTab.put("func", "onScriptLoad");
                    logTab.put("script", "logging.js");
                    tabsArray.put(logTab);
                }

                /* Закладка пользователей */
                if (Privileges.USERS.name().equals(priv.getPrivilege().name())) {
                    Template subsTemplate = engine.getTemplate("velocity/users.vm", "UTF-8");
                    VelocityContext subsVelocityContext = new VelocityContext();
                    StringWriter writer = new StringWriter();
                    subsTemplate.merge(subsVelocityContext, writer);
                    JSONObject sourceTab = new JSONObject();
                    sourceTab.put("name", "Пользователи");
                    sourceTab.put("content", writer.toString());
                    sourceTab.put("func", "renderUsers");
                    sourceTab.put("script", "users.js");
                    tabsArray.put(sourceTab);
                }
            }
        }

        resp.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = resp.getWriter()) {
            out.println(tabsArray.toString());
        }
    }
}
