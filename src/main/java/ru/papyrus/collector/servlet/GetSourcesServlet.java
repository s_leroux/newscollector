package ru.papyrus.collector.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.papyrus.collector.main.App;

/**
 * Получаем список источников в зависимости от привилегий пользователя
 * @author roux
 */
@WebServlet(value="/NewsCollector/getSources")
public class GetSourcesServlet extends HttpServlet{
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    /* Получаем список имён источников - в зависимости от привилегий пользователя сессии */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        LOGGER.debug("Это GetSourcesServlet doGet");
        String sessionId = req.getSession().getId();
        JSONArray names = App.getInstance().getSourceDAO().retrieveSourceNames(sessionId);
//        LOGGER.debug("Получено имён: " + names.length());
//        for (int i = 0; i < names.length(); i++) {
//            LOGGER.debug("имя " + i + ": " + names.getString(i));
//        }

        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        try {
//            LOGGER.debug("Печатаются имена источников: " + names.toString());
            out.println(names.toString());
        } finally {
            out.close();
        }
    }

    /**
     * Получаем источники
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        LOGGER.debug("Это GetSourcesServlet doPost");

        String namesList = req.getParameter("names");
//        LOGGER.debug("namesList: " + namesList);
        
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
//            LOGGER.debug("Выдаём источники по выбору.");
        JSONArray namesArray = new JSONArray(namesList);
//            LOGGER.debug("namesArray: " + namesArray.length());
//            for (int i = 0; i < namesArray.length(); i++) {
//                LOGGER.debug("name string: " + namesArray.getString(i));
//            }
        JSONArray sources = App.getInstance().getSourceDAO().retrieveSourcesByName(namesArray);
        try {
//                LOGGER.debug("Печатаются источники: " + sources.toString(4));
            out.println(sources.toString(4));
        } finally {
            out.close();
        }
    }
}
