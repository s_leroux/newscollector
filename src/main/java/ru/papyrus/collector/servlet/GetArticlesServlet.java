package ru.papyrus.collector.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import javax.servlet.ServletConfig;
import org.json.JSONObject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import ru.papyrus.collector.dao.ArticleDAO;
import ru.papyrus.collector.main.App;

/**
 *
 * @author roux
 */
@WebServlet(value="/NewsCollector/getArticles")
public class GetArticlesServlet extends HttpServlet{
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        this.articleDAO = App.getInstance().getArticleDAO();
    }
    
    private ArticleDAO articleDAO;
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        LOGGER.debug("Это GetArticlesServlet doPost, articlesCount: " + req.getParameter("articlesCount"));

        JSONObject ret = articleDAO.retrieveArticles(req.getParameterMap(), req.getSession());

//        LOGGER.debug("Всего найдено статей: " + ret.getLong("count"));
//        LOGGER.debug("Получено статей: " + ret.getJSONArray("articles").length());

        resp.setContentType("text/html;charset=UTF-8");
        
        PrintWriter out = resp.getWriter();
        try {
            out.println(ret.toString());
        } finally {
            out.close();
        }
    }
}
