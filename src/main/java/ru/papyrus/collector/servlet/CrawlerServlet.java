/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.papyrus.collector.servlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ru.papyrus.collector.main.App;

/**
 *
 * @author roux
 */
@WebServlet(value="/NewsCollector/start", loadOnStartup=1)
public class CrawlerServlet extends HttpServlet{
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    
//    @Autowired
//    private App app;
    /**
     * Конструктор без параметров
     */
    public CrawlerServlet() {
        new ClassPathXmlApplicationContext("beans.xml");
//        LOGGER.debug("Это CrawlerServlet");
    }
}
