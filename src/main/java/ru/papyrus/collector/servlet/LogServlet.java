package ru.papyrus.collector.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import ru.papyrus.collector.dao.LogDAO;
import ru.papyrus.collector.main.App;
import ru.papyrus.collector.main.logger.DBLogger;

/**
 *
 * @author roux
 */
@WebServlet(value="/NewsCollector/getLog")
public class LogServlet extends HttpServlet{
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    
    private LogDAO logDAO;

    @Override
    public void init() throws ServletException {
        super.init();
        this.logDAO = App.getInstance().getLogDAO();
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doStuff(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doStuff(req, resp);
    }
    
    private void doStuff(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        LOGGER.debug("Это doStuff, req term: " + req.getParameter("term"));
        String dateString = req.getParameter("term");
        /* Получаем лог в соответствии с заданными параметрами */
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = sdf.parse(dateString);
        } catch (ParseException e) {
            DBLogger.getInstance().error("Не удалось распознать дату " + dateString);
            date = new Date();
        }
        JSONArray ret = logDAO.getLogByDate(date);
        
        resp.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = resp.getWriter()) {
            out.println(ret.toString());
        }
    }
}
