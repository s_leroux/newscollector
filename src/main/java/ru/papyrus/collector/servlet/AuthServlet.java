package ru.papyrus.collector.servlet;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.papyrus.collector.entity.UserSession;
import ru.papyrus.collector.entity.User;
import ru.papyrus.collector.main.App;
import ru.papyrus.collector.main.utils.Util;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;

@WebServlet(value="/NewsCollector/getAuth")
public class AuthServlet extends HttpServlet {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /* Сделать проверку по БД */
        String authCode = req.getParameter("authCode");

        String decoded = new String(Base64.getDecoder().decode(authCode)) ;

        /* Возвращаемый JSON */
        JSONObject ret = new JSONObject();

        /* Проверяем наличие логина и пароля */
        if (decoded.indexOf(":") == -1) {
//            LOGGER.debug("Не удалось найти пару логин/пароль.");
            ret.put("success", false);
            ret.put("message", "Не удалось найти пару логин/пароль.");
        } else {
//            LOGGER.debug("Пара логин/пароль найдена.");
            String[] loginPassword = decoded.split("\\:");
//            LOGGER.debug("loginPassword: " + loginPassword.length);
            if (loginPassword[0].length() <= 0 || loginPassword[1].length() <= 0) {
                ret.put("success", false);
                ret.put("message", "Не удалось найти пару логин/пароль.");
            } else {
                User user = App.getInstance().getUsersDAO().getUserByLogin(loginPassword[0].toLowerCase());
//                LOGGER.debug("Найден user по логину " + loginPassword[0] + ": " + user);
                if(user == null) {
                    ret.put("success", false);
                    ret.put("message", "Неверный логин или пароль. (не найден пользователь)");
                } else {
                    /* Проверяем пароль */
                    byte[] pwdHash = Util.getPwdHash(loginPassword[1], user.getSalt());
                    if(Arrays.equals(pwdHash, user.getPasswordHash())) {
                        /* Надо как-то записать сессию. */
//                        LOGGER.debug("Записываем сессию, user FamilyName: " + user.getFamilyName());
                        boolean success = App.getInstance().getSessionDAO().addSession(
                                new UserSession(req.getSession().getId(), user, new Date())
                        );
                        /* Если по каким-то причинам сессию записать не удалось - и фиг с ней. */

                        ret.put("success", true);
                    } else {
                        ret.put("success", false);
                        ret.put("message", "Неверный логин или пароль.");
                    }
                }
            }
        }

        resp.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = resp.getWriter()) {
            out.println(ret.toString());
        }
    }
}
