package ru.papyrus.collector.servlet;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.papyrus.collector.dao.SessionDAO;
import ru.papyrus.collector.entity.UserSession;
import ru.papyrus.collector.main.App;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 */
@WebServlet(value="/NewsCollector/getMenuButtons")
public class GetMenuButtonsServlet extends HttpServlet {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private SessionDAO sessionDAO;

    @Override
    public void init() throws ServletException {
        super.init();
        this.sessionDAO = App.getInstance().getSessionDAO();
//        LOGGER.debug("sessionDAO: " + sessionDAO);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        LOGGER.debug("Это GetMenuButtonsServlet doPost, RequestedSessionId: " + req.getRequestedSessionId());
        /* Изначально присутствует только кнопка "О программе" */
        /* Если вход не выполнен, то добавляем "Войти", "Зарегистрироваться" */
        /* Если вход выполнен, то добавляем к ней "Выйти", "Профиль" */

        JSONArray arr = new JSONArray();
        /* Проверяем наличие сессии */
        UserSession session = sessionDAO.getSessionById(req.getSession().getId());
//        LOGGER.debug("session: " + session);
        if (session != null) {
            /* Сессия есть */
            arr.put(getButtonObject("logOutButton", session.getUser().toString() + " - выход...", "logOutAction()", "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjE2cHgiIGhlaWdodD0iMTZweCIgdmlld0JveD0iMCAwIDk2Ljk0MyA5Ni45NDMiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDk2Ljk0MyA5Ni45NDM7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPGc+Cgk8Zz4KCQk8cGF0aCBkPSJNNjEuMTY4LDgzLjkySDExLjM2NFYxMy4wMjVINjEuMTdjMS4xMDQsMCwyLTAuODk2LDItMlYzLjY2YzAtMS4xMDQtMC44OTYtMi0yLTJIMmMtMS4xMDQsMC0yLDAuODk2LTIsMnY4OS42MjMgICAgYzAsMS4xMDQsMC44OTYsMiwyLDJoNTkuMTY4YzEuMTA1LDAsMi0wLjg5NiwyLTJWODUuOTJDNjMuMTY4LDg0LjgxNCw2Mi4yNzQsODMuOTIsNjEuMTY4LDgzLjkyeiIgZmlsbD0iIzg2N2I1OSIvPgoJCTxwYXRoIGQ9Ik05Ni4zNTUsNDcuMDU4bC0yNi45MjItMjYuOTJjLTAuNzUtMC43NTEtMi4wNzgtMC43NS0yLjgyOCwwbC02LjM4Nyw2LjM4OGMtMC43ODEsMC43ODEtMC43ODEsMi4wNDcsMCwyLjgyOCAgICBsMTIuMTYsMTIuMTYySDE5LjczN2MtMS4xMDQsMC0yLDAuODk2LTIsMnY5LjkxMmMwLDEuMTA0LDAuODk2LDIsMiwyaDUyLjY0NEw2MC4yMjEsNjcuNTljLTAuNzgxLDAuNzgxLTAuNzgxLDIuMDQ3LDAsMi44MjggICAgbDYuMzg3LDYuMzg5YzAuMzc1LDAuMzc1LDAuODg1LDAuNTg2LDEuNDE0LDAuNTg2YzAuNTMxLDAsMS4wMzktMC4yMTEsMS40MTQtMC41ODZsMjYuOTIyLTI2LjkyICAgIGMwLjM3NS0wLjM3NSwwLjU4Ni0wLjg4NSwwLjU4Ni0xLjQxNEM5Ni45NDMsNDcuOTQxLDk2LjczLDQ3LjQzMyw5Ni4zNTUsNDcuMDU4eiIgZmlsbD0iIzg2N2I1OSIvPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo="));
            arr.put(getButtonObject("profileButton", session.getUser().toString() + " - профиль...", "profileAction()", "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjE2cHgiIGhlaWdodD0iMTZweCIgdmlld0JveD0iMCAwIDk1LjkyNiA5NS45MjYiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDk1LjkyNiA5NS45MjY7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPGc+Cgk8Zz4KCQk8cGF0aCBkPSJNNDcuODkzLDQ3LjIyMWMxMS43NjgsMCwyMS4zNDEtMTAuNTkyLDIxLjM0MS0yMy42MTFTNTkuNjYsMCw0Ny44OTMsMEMzNi4xMjUsMCwyNi41NSwxMC41OTIsMjYuNTUsMjMuNjEgICAgQzI2LjU1LDM2LjYzLDM2LjEyNSw0Ny4yMjEsNDcuODkzLDQ3LjIyMXoiIGZpbGw9IiM4NjdiNTkiLz4KCQk8cGF0aCBkPSJNNzIuNDc3LDQ0LjEyM2MtMS4yNDQtMC4yNjktMi41MjQsMC4yNzItMy4xOTIsMS4zNTVDNjEuNjUsNTcuODQ3LDQ5LjM0LDU4LjIwNCw0Ny45NjIsNTguMjA0ICAgIHMtMTMuNjg3LTAuMzU3LTIxLjMyLTEyLjcyMmMtMC42Ny0xLjA4NS0xLjk1My0xLjYyOC0zLjE5OC0xLjM1NEM2Ljg2OCw0Ny43NzcsMi40OTcsNzIuNzk4LDMuNzg5LDkzLjExNSAgICBjMC4xMDEsMS41OCwxLjQxMSwyLjgxMSwyLjk5NCwyLjgxMWg4Mi4zNmMxLjU4MywwLDIuODk0LTEuMjMsMi45OTMtMi44MTFDOTMuNDI5LDcyLjc3NSw4OS4wNTcsNDcuNzQsNzIuNDc3LDQ0LjEyM3oiIGZpbGw9IiM4NjdiNTkiLz4KCTwvZz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K"));
        } else {
            /* Сессии нет */
            arr.put(getButtonObject("logInButton", "Войти...", "logInAction()", "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjE2cHgiIGhlaWdodD0iMTZweCIgdmlld0JveD0iMCAwIDk1LjY2NyA5NS42NjciIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDk1LjY2NyA5NS42Njc7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPGc+Cgk8Zz4KCQk8cGF0aCBkPSJNMzkuMTczLDcyLjM0NGwzOS40NDctMjIuNzc3YzAuNjE5LTAuMzU2LDEtMS4wMTgsMS0xLjczMXMtMC4zODEtMS4zNzUtMS0xLjczMkwzOS4xNzMsMjMuMzI0ICAgIGMtMC42MTktMC4zNTctMS4zODEtMC4zNTctMiwwYy0wLjYxOSwwLjM1Ny0xLDEuMDE4LTEsMS43MzJ2MTAuNjA1SDIuMTIxYy0xLjEwNCwwLTIsMC44OTYtMiwydjIwLjM0NGMwLDEuMTA0LDAuODk2LDIsMiwyICAgIGgzNC4wNTN2MTAuNjA0YzAsMC43MTYsMC4zODEsMS4zNzUsMSwxLjczMmMwLjMxLDAuMTgsMC42NTUsMC4yNjgsMSwwLjI2OEMzOC41MTksNzIuNjA5LDM4Ljg2NCw3Mi41MjEsMzkuMTczLDcyLjM0NHoiIGZpbGw9IiM4NjdiNTkiLz4KCQk8cGF0aCBkPSJNODAuNzc1LDBINDAuMDI2Yy0xLjEwNCwwLTIsMC44OTYtMiwydjZjMCwxLjEwNCwwLjg5NiwyLDIsMmg0MC43NDljMi42MzIsMCw0Ljc3MSwyLjE0MSw0Ljc3MSw0Ljc3MXY2Ni4xMjUgICAgYzAsMi42MzEtMi4xNDEsNC43NzEtNC43NzEsNC43NzFINDAuMDI2Yy0xLjEwNCwwLTIsMC44OTYtMiwydjZjMCwxLjEwNCwwLjg5NiwyLDIsMmg0MC43NDljOC4xNDYsMCwxNC43NzEtNi42MjcsMTQuNzcxLTE0Ljc3MSAgICBWMTQuNzcyQzk1LjU0Niw2LjYyNyw4OC45MiwwLDgwLjc3NSwweiIgZmlsbD0iIzg2N2I1OSIvPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo="));
            arr.put(getButtonObject("registerButton", "Зарегистрироваться...", "registerAction()", "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjE2cHgiIGhlaWdodD0iMTZweCIgdmlld0JveD0iMCAwIDk4LjE2NyA5OC4xNjciIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDk4LjE2NyA5OC4xNjc7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPGc+Cgk8Zz4KCQk8cGF0aCBkPSJNODAuNzA4LDEyLjY3di0yLjEyN2MwLTEuMDAxLTAuODEyLTEuODEzLTEuODEzLTEuODEzSDEuODEzQzAuODEyLDguNzMsMCw5LjU0MywwLDEwLjU0M3Y3Ny4wOCAgICBjMCwxLjAwMiwwLjgxMSwxLjgxMywxLjgxMywxLjgxM2g3Ny4wODJjMS4wMDIsMCwxLjgxMy0wLjgxMywxLjgxMy0xLjgxM1Y0OC43NzZ2LTEuMDA5bC05LjY1Niw5LjY1NHYyMi4zNTdIOS42NTdWMTguMzg2aDYxLjM5NSAgICB2My45NEw4MC43MDgsMTIuNjd6IiBmaWxsPSIjODY3YjU5Ii8+CgkJPHBhdGggZD0iTTQ3LjY1NCw1MC4zMDNjLTAuMTksMC4xODktMC4zMDMsMC40NDMtMC4zMTUsMC42OTNsLTEuODQ3LDEyLjg1MWMtMC4wNDUsMC4zMzgsMC4wNywwLjY4NSwwLjMxMiwwLjkyNiAgICBjMC4yMDcsMC4yMDYsMC40OCwwLjMxOSwwLjc3MywwLjMxOWMwLjA1MywwLDAuMTA0LTAuMDA0LDAuMTU2LTAuMDEybDEyLjc5Mi0xLjg0YzAuMjg1LTAuMDA3LDAuNTQ3LTAuMTE5LDAuNzU0LTAuMzI4ICAgIGwyOS4wMi0yOS4wMjZsLTEyLjYwOS0xMi42MUw0Ny42NTQsNTAuMzAzeiIgZmlsbD0iIzg2N2I1OSIvPgoJCTxwYXRoIGQ9Ik05OC4xNjcsMjQuNTYyYzAtMC4yOTMtMC4xMTQtMC41NjctMC4zMjEtMC43NzJMODYuNzg1LDEyLjcyOWMtMC4zOTUtMC4zOTktMS4xNS0wLjM5OS0xLjU0Ny0wLjAwMWwtNS4yMjEsNS4yMjEgICAgbDEyLjYwOCwxMi42MDlsNS4yMjItNS4yMjNDOTguMDUzLDI1LjEyOCw5OC4xNjcsMjQuODU0LDk4LjE2NywyNC41NjJ6IiBmaWxsPSIjODY3YjU5Ii8+Cgk8L2c+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg=="));
        }

//        LOGGER.debug("arr: " + arr);

        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        try {
            out.println(arr.toString());
        } finally {
            out.close();
        }
    }

    private JSONObject getButtonObject(String id, String title, String action, String icon) {
        JSONObject buttonObject = new JSONObject();
        buttonObject.put("id", id);
//        buttonObject.put("class", "button menu-button");
        buttonObject.put("title", title);
        buttonObject.put("onclick", action);
        buttonObject.put("icon", icon);
        return buttonObject;
    }
}