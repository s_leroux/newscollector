package ru.papyrus.collector.servlet;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.papyrus.collector.entity.*;
import ru.papyrus.collector.main.App;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@WebServlet(value="/NewsCollector/getUserData")
public class UserDataServlet extends HttpServlet {
//    private static Logger LOGGER = LoggerFactory.getLogger(UserDataServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        JSONObject ret = new JSONObject();

        String sessionId = req.getSession().getId();

        UserSession userSession = App.getInstance().getSessionDAO().getSessionById(sessionId);

        if(userSession != null) {
            User user = userSession.getUser();
            JSONObject userJSON = new JSONObject();
            userJSON.put("login", user.getLogin());
            userJSON.put("familyName", user.getFamilyName());
            userJSON.put("name", user.getName());

            StringBuilder privilegesSB = new StringBuilder();
            for (UserPrivilege privilege: user.getUserPrivileges()) {
                if (privilegesSB.length() > 0) {
                    privilegesSB.append(" ");
                }
                privilegesSB.append(privilege.getPrivilege().name());
            }
            userJSON.put("privileges", privilegesSB.toString());

            /* Взять все персональные доступы и добавить к ним общедоступные (если ещё нет) без подписки */
            Collection<Source> accessibleSources = App.getInstance().getSourceDAO().getCommonSources();
            JSONArray accessesJSON = new JSONArray();
            Map<Source, Boolean> sourcesAndSubs = new HashMap<>(0);
            for (Access access: user.getAccesses()) {
                sourcesAndSubs.put(access.getSource(), access.getSubscription());
//                JSONObject accessJSON = new JSONObject();
//                accessJSON.put("source", access.getSource().getName());
//                accessJSON.put("subs", access.getSubscription());
//                accessesJSON.put(accessJSON);
            }
            for (Source source: accessibleSources) {
                if (!sourcesAndSubs.containsKey(source)) {
                    sourcesAndSubs.put(source, false);
                }
            }
            for (Source source: sourcesAndSubs.keySet()) {
                JSONObject accessJSON = new JSONObject();
                accessJSON.put("source", source.getName());
                accessJSON.put("desc", source.getDescription());
                accessJSON.put("subs", sourcesAndSubs.get(source));
                accessJSON.put("id", source.getId());
                accessesJSON.put(accessJSON);
            }
            userJSON.put("accesses", accessesJSON);

            ret.put("user", userJSON);
        }

        resp.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = resp.getWriter()) {
            out.println(ret.toString());
        }
    }
}