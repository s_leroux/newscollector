/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.papyrus.collector.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Timer;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.papyrus.collector.dao.SourceDAO;
import ru.papyrus.collector.entity.Source;
import ru.papyrus.collector.main.App;
import ru.papyrus.collector.main.logger.DBLogger;
import ru.papyrus.collector.main.logger.Marker;

/**
 *
 * @author roux
 */
@WebServlet(value="/NewsCollector/importSources")
public class ImportServlet extends HttpServlet{
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    
    private SourceDAO sourceDAO;

    private DBLogger DB_LOGGER = App.getInstance().getDB_LOGGER();
    
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        this.sourceDAO = App.getInstance().getSourceDAO();
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        LOGGER.debug("Это ImportServlet doPost");
        String sources = req.getParameter("sources");
//        LOGGER.debug("req sources: " + sources);
        boolean forceUpdate = Boolean.valueOf(req.getParameter("forceUpdate"));
//        LOGGER.debug("req forceUpdate: " + forceUpdate);

        JSONArray sourcesToImport = new JSONArray(sources);
//        LOGGER.debug("sourcesToImport: " + sourcesToImport.toString(4));

        JSONArray reply = new JSONArray();
        
        boolean restartNeeded = false;
        for (int i = 0; i < sourcesToImport.length(); i++) {
//            LOGGER.debug("sourcesToImport " + i + ": " + sourcesToImport.get(i));

            /* Проверяем наличие */
            String name = ((JSONObject)sourcesToImport.get(i)).getString("name");
//            LOGGER.debug("name: " + name);

            Marker marker = new Marker(name);
            
            JSONArray r = sourceDAO.retrieveSourcesByName(new JSONArray().put(name));
//            LOGGER.debug("Получен ответ от БД на запрос " + name + ": " + r.length());
            if(r.length() <= 0) {
                /* Таких нет, надо писать в любом случае */
                JSONObject newSourceJSON = (JSONObject)sourcesToImport.get(i);
                Source newSource = new Source(newSourceJSON);
                boolean success = sourceDAO.addSource(newSource);
                if (success) {
                    DB_LOGGER.info(marker, "Добавлен источник " + name + ".");
                    reply.put(new JSONObject().put("name", name).put("status", "STORED"));
                    /* Перезапустить источники */
                    restartNeeded = true;
                } else {
                    DB_LOGGER.error(marker, "Не удалось добавить источник " + name + ".");
                    reply.put(new JSONObject().put("name", name).put("status", "ERROR"));
                }
            } else {
                if(forceUpdate) {
                    /* Надо переписать существующий источник */
//                    LOGGER.debug("Принудительно переписываем " + name);
//                    DB_LOGGER.info(marker, "Переписывается источник " + name + ".");

                    JSONObject sourceJSON = (JSONObject)sourcesToImport.get(i);
//                    LOGGER.debug("sourceJSON: " + sourceJSON.toString(4));
                    Source source = null;
                    try {
                        int id = r.getJSONObject(0).getInt("id");
                        source = new Source(sourceJSON);
                        source.setId(id);
                    } catch (Exception e) {
                        DB_LOGGER.error(marker, "Ошибка при создании объекта Source (" + name + ") " + e.getClass().getSimpleName() + ": " + e.getMessage()) ;
                        reply.put(new JSONObject().put("name", name).put("status", "ERROR"));
                        continue;
                    }
                    
//                    LOGGER.debug("source: " + source);
                    boolean success = sourceDAO.updateSource(source);
//                    LOGGER.debug("success: " + success);

                    if(success) {
                        DB_LOGGER.info(marker, "Переписан источник " + name + ".");
                        reply.put(new JSONObject().put("name", name).put("status", "UPDATED"));
                        /* Перезапустить проход по источнику */
                        restartNeeded = true;
                    } else {
                        DB_LOGGER.error(marker, "Не удалось переписать источник " + name + ".");
                        reply.put(new JSONObject().put("name", name).put("status", "ERROR"));
                    }
                } else {
                    /* Нужно проигнорировать */
//                    LOGGER.debug("Игнорируем");
                    reply.put(new JSONObject().put("name", name).put("status", "IGNORED"));
                }
            }
        }
        if (restartNeeded) {
            DB_LOGGER.debug("Перезапускаем таймер");
            App.getInstance().runFetchersTimer();
        }
        
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        try {
            out.println(reply.toString(4));
        } finally {
            out.close();
        }
        
        /* Записали. Теперь перезапустить таймер. */
//        Timer timer = App.getInstance().getFetchersTimer();
//        timer.cancel(); //Terminates this timer,discarding any currently scheduled tasks.
//        timer.purge();  // Removes all cancelled tasks from this timer's task queue.
//
//        App.getInstance().setFetchersTimer(new Timer());
//
//        App.getInstance().init();
    }
}
