package ru.papyrus.collector.servlet;

import org.json.JSONObject;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import ru.papyrus.collector.dao.SessionDAO;
import ru.papyrus.collector.main.App;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Выход из системы
 */
@WebServlet(value="/NewsCollector/logout")
public class LogoutServlet extends HttpServlet {
//    private static final Logger LOGGER = LoggerFactory.getLogger(LogoutServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        String sessionId = req.getSession().getId();
//        LOGGER.debug("Это выход, sessionId: " + sessionId);

        /* Возвращаемый JSON */
        JSONObject ret = new JSONObject();

        SessionDAO sessionDAO = App.getInstance().getSessionDAO();

        boolean success = sessionDAO.deleteSessionById(sessionId);
//        LOGGER.debug("EXIT success: " + success);

        ret.put("success", success);

        if(!success) {
            ret.put("message", "Ошибка на сервере при выходе из системы");
        }

        resp.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = resp.getWriter()) {
            out.println(ret.toString());
        }
    }
}
