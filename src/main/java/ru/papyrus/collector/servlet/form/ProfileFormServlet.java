package ru.papyrus.collector.servlet.form;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.papyrus.collector.entity.Access;
import ru.papyrus.collector.entity.Source;
import ru.papyrus.collector.entity.UserPrivilege;
import ru.papyrus.collector.entity.UserSession;
import ru.papyrus.collector.main.App;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;

@WebServlet(value = "/NewsCollector/profileForm")
public class ProfileFormServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProfileFormServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserSession userSession = App.getInstance().getSessionDAO().getSessionById(req.getSession().getId());
//        LOGGER.debug("Это ProfileFormServlet doPost, userSession: " + userSession);

        VelocityEngine engine = App.getInstance().getVelocityEngineFactory().getObject();

        VelocityContext sourcesVelocityContext = new VelocityContext();

        StringWriter writer = new StringWriter();

        /* А можно различать, когда сессия есть и сессии нет, и в зависимости от этого выдавать форму профиля или регистрации. */
        Template sourcesTemplate = null;
        /* Берём список источников */
        Collection<Source> accessibleSources = App.getInstance().getSourceDAO().getCommonSources();
        if (userSession == null) {
            sourcesTemplate = engine.getTemplate("velocity/register.vm", "UTF-8");
            sourcesVelocityContext.put("sourceList", accessibleSources);
        } else {
            sourcesTemplate = engine.getTemplate("velocity/profile.vm", "UTF-8");
        }

        sourcesTemplate.merge(sourcesVelocityContext, writer);

        resp.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = resp.getWriter()) {
            out.println(writer.toString());
        }
    }
}
