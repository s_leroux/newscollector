package ru.papyrus.collector.servlet.form.helper;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;

@WebServlet(value = "/NewsCollector/getStartPage")
public class StartPageGetterServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(StartPageGetterServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        LOGGER.debug("Это StartPageGetterServlet");

        URL url = new URL(req.getParameter("url")) ;
//        LOGGER.debug("url: " + url.toString());

        Charset encoding = null;
        try {
            encoding = Charset.forName(req.getParameter("enc")) ;
        } catch (UnsupportedCharsetException | IllegalCharsetNameException e) {
            /* Оставляем null */
            LOGGER.debug("Передана неправильная кодировка");
        } catch (Exception e) {
            LOGGER.error("Вместо кодировки вообще передано невесть что, " + e.getClass() + ", " + e.getMessage());
        }

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.connect();
        int responseCode = conn.getResponseCode();

//        InputStream is = conn.getInputStream();
        /* Берём кодировку с сайта */
        Charset enc = Charset.forName(StringUtils.substringAfterLast(conn.getContentType(), "charset="));
//        LOGGER.debug("Кодировка с сайта: " + enc);
//        LOGGER.debug("Передана кодировка: " + encoding);

        JSONObject ret = new JSONObject();

        /* Если кодировка не найдена и не передана или передана некорректная, используем UTF-8 */
        if (enc != null) {
            /* Если кодировка найдена, используем её */
        } else {
            /* Если кодировка не найдена, используем переданную (если она есть) */
            if (encoding != null) {
                enc = encoding;
            } else {
                enc = Charset.forName("UTF-8");
            }
        }

        if(responseCode == 200) {
            ret.put("success", true);
            ret.put("encoding", enc);
            /* Получить содержимое */
//            ret.put("content", IOUtils.toString(conn.getInputStream(), enc));
        } else {
            ret.put("success", false);
            ret.put("responseCode", responseCode);
        }

        conn.disconnect();

        resp.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = resp.getWriter()) {
            out.println(ret.toString());
        }
    }
}
