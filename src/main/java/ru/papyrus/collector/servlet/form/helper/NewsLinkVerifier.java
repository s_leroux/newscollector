package ru.papyrus.collector.servlet.form.helper;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.DomSerializer;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Random;

@WebServlet(value = "/NewsCollector/checkNewsLink")
public class NewsLinkVerifier extends HttpServlet{
    private static final Logger LOGGER = LoggerFactory.getLogger(NewsLinkVerifier.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
//        LOGGER.debug("Это NewsLinkVerifier doPost");

        String startPage = req.getParameter("startPage");
        String encoding = req.getParameter("encoding");
        String newsPageLinkXPath = req.getParameter("newsPageLinkXPath");
        String newsLinkXPath = req.getParameter("newsLinkXPath");
//        LOGGER.debug("startPage: " + startPage + ", encoding: " + encoding + ", pageLinkXPath: " + newsPageLinkXPath + ", newsLinkXPath: " + newsLinkXPath);

        JSONObject json = process(startPage, encoding, newsPageLinkXPath, newsLinkXPath);

        resp.setContentType("text/html;charset=UTF-8");
//        resp.setStatus(HttpStatus.OK.value());
        try (PrintWriter out = resp.getWriter()) {
            out.println(json.toString());
        } catch (Exception e) {
            LOGGER.error("Exception", e);
        }
    }

    private JSONObject process(String startPage, String encoding, String newsPageLinkXPath, String newsLinkXPath) {
//        LOGGER.debug("Это process");
        JSONObject ret = new JSONObject();

        String newsPageAddress;

        /* Итак, проверям XPath по нахождению ссылки на новостную статью */
        /* Распадается на два случая - если XPath ссылки на страничку новостей есть или если его нет */
        if(!StringUtils.isEmpty(newsPageLinkXPath)) {
            /* XPath на страничку новостей есть */
            /* Определяем страничку, где будем искать новости.
            Для этого в изрядной мере повторяем то, что уж было сделано при проверке XPath на страничку новостей. */
            /* Находим страничку для поиска ссылок на новости */
            try {
                org.jsoup.nodes.Document doc = Jsoup.connect(startPage).get();
                String body = doc.body().outerHtml();
                HtmlCleaner cleaner = new HtmlCleaner();
                TagNode cleaned = cleaner.clean(IOUtils.toInputStream(body, encoding));
                Document document = new DomSerializer(new CleanerProperties()).createDOM(cleaned);
                XPath xpath = XPathFactory.newInstance().newXPath();
                NodeList newsPageNL = (NodeList) xpath.evaluate(newsPageLinkXPath, document, XPathConstants.NODESET);
                String textContent = newsPageNL.item(0).getTextContent();
//                LOGGER.debug("textContent: " + textContent);
                /* Пробуем открыть */
                /* Следует учесть, что может быть как абсолютный, так и относительный путь */
                URL url;
                if (new URI(textContent).isAbsolute()) {
                    url = new URL(textContent);
                } else {
                    url = new URL(new URL(startPage), textContent);
                }

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                LOGGER.debug("url: " + url.toString());
//                LOGGER.debug("conn.getResponseCode: " + conn.getResponseCode()) ;
                if (HttpStatus.OK.value() == conn.getResponseCode()) {
                    newsPageAddress = url.toString();
                } else {
                    ret.put("success", false);
                    ret.put("message", "Не удалось открыть страничку новостей" + textContent + ".");
                    return ret;
                }
            } catch (IOException e) {
                ret.put("success", false);
                ret.put("message", "Ошибка при получении стартовой страницы.");
                return ret;
            } catch (ParserConfigurationException e) {
                ret.put("success", false);
                ret.put("message", "Ошибка при разборе стартовой страницы.");
                return ret;
            } catch (XPathExpressionException e) {
                ret.put("success", false);
                ret.put("message", "Ошибка XPath для для поиска ссылки на страничку новостей.");
                return ret;
            } catch (URISyntaxException e) {
                ret.put("success", false);
                ret.put("message", "Ошибка определения ссылки на страничку новостей.");
                return ret;
            }
        } else {
            /* XPath на страничку новостей нет */
            /* В качестве странички, где будем искать новости, используем стартовую. */
            newsPageAddress = startPage;
        }

        /* Проверяем, находится ли ссылка на новостную статью по XPath  */
        try {
//            LOGGER.debug("Проверяем страничку {} на предмет поиска ссылки но новость.", newsPageAddress);
            org.jsoup.nodes.Document doc = Jsoup.connect(newsPageAddress).get();
            String body = doc.body().outerHtml();
            HtmlCleaner cleaner = new HtmlCleaner();
            TagNode cleaned = cleaner.clean(IOUtils.toInputStream(body, encoding));
            Document document = new DomSerializer(new CleanerProperties()).createDOM(cleaned);
            XPath xpath = XPathFactory.newInstance().newXPath();
            NodeList newsNL = (NodeList) xpath.evaluate(newsLinkXPath, document, XPathConstants.NODESET);
            if (newsNL.getLength() <= 0) {
                ret.put("success", false);
                ret.put("message", "По заданному XPath не найдено ссылок на новостные статьи.");
                return ret;
            } else {
                /* Ссылки найдены, продолжаем - открываем случайную */
                Random random = new Random(new Date().getTime());
                int linkNo = random.nextInt(newsNL.getLength());
                String textContent = newsNL.item(linkNo).getTextContent();
//                LOGGER.debug("textContent: " + textContent);
                /* Пробуем открыть */
                URL url;
                if (new URI(textContent).isAbsolute()) {
                    url = new URL(textContent);
                } else {
                    url = new URL(new URL(startPage), textContent);
                }
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                if (HttpStatus.OK.value() == conn.getResponseCode()) {
                    /* Берём кодировку с сайта */
                    Charset enc = Charset.forName(StringUtils.substringAfterLast(conn.getContentType(), "charset="));
                    /* Если кодировка не найдена и не передана или передана некорректная, используем UTF-8 */
                    if (enc == null) {
                        /* Если кодировка не найдена, используем переданную (если она есть) */
                        if (encoding != null) {
                            enc = Charset.forName(encoding);
                        } else {
                            enc = Charset.forName("UTF-8");
                        }
                    }

                    ret.put("success", true);
                    ret.put("content", IOUtils.toString(conn.getInputStream(), enc));
                    return ret;
                } else {
                    ret.put("success", false);
                    ret.put("message", "Не удалось открыть новостную страничку " + textContent + ".");
                    return ret;
                }
            }
        } catch (IOException e) {
            LOGGER.debug("IOException", e);
            ret.put("success", false);
            ret.put("message", "Ошибка при получении страницы для поиска новостной статьи " + newsPageAddress);
            return ret;
        } catch (ParserConfigurationException e) {
            ret.put("success", false);
            ret.put("message", "Ошибка при разборе страницы для поиска новостной статьи " + newsPageAddress + ".");
            return ret;
        } catch (XPathExpressionException e) {
            ret.put("success", false);
            ret.put("message", "Ошибка XPath для для поиска новостной статьи .");
            return ret;
        } catch (URISyntaxException e) {
            ret.put("success", false);
            ret.put("message", "Ошибка определения ссылки на новостную страничку.");
            return ret;
        }
    }
}