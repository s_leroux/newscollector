package ru.papyrus.collector.servlet.form;


import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.papyrus.collector.dao.SourceDAO;
import ru.papyrus.collector.entity.Source;
import ru.papyrus.collector.main.App;
import ru.papyrus.collector.main.fetcher.Fetcher;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Set;

@WebServlet(value = "/NewsCollector/editSourceForm")
public class EditSourceFormServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(EditSourceFormServlet.class);

    private SourceDAO sourceDAO;

    @Override
    public void init() throws ServletException {
        super.init();

        this.sourceDAO = App.getInstance().getSourceDAO();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//        super.doPost(req, resp);

//        LOGGER.debug("Это EditSourceFormServlet, source: " + req.getParameter("source"));
        Source source = sourceDAO.getSourceByName(req.getParameter("source"));

        VelocityEngine engine = App.getInstance().getVelocityEngineFactory().getObject();

        VelocityContext editSourcesVelocityContext = new VelocityContext();
        StringWriter writer = new StringWriter();
        Template editSourceTemplate = engine.getTemplate("velocity/editSource.vm", "UTF-8");

        editSourcesVelocityContext.put("source", source);

        /* Найти все классы, наследующие ru.papyrus.collector.main.fetcher.Fetcher */
        Reflections reflections = new Reflections("ru.papyrus.collector.main.fetcher");
        Set<Class<? extends Fetcher>> classes = reflections.getSubTypesOf(Fetcher.class);
        editSourcesVelocityContext.put("classes", classes);

        editSourceTemplate.merge(editSourcesVelocityContext, writer);

        resp.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = resp.getWriter()) {
            out.println(writer.toString());
        }
    }
}
