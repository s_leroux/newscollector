package ru.papyrus.collector.servlet.form;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.papyrus.collector.main.App;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

@WebServlet(value = "/NewsCollector/deleteSourcesForm")
public class DeleteSourcesFormServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteSourcesFormServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//        LOGGER.debug("Это DeleteSourcesFormServlet");

        VelocityEngine engine = App.getInstance().getVelocityEngineFactory().getObject();

        VelocityContext deleteSourcesVelocityContext = new VelocityContext();

        StringWriter writer = new StringWriter();

        Template deleteSourcesTemplate = engine.getTemplate("velocity/deleteSources.vm", "UTF-8");

        deleteSourcesTemplate.merge(deleteSourcesVelocityContext, writer);

        resp.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = resp.getWriter()) {
            out.println(writer.toString());
        }
    }
}
