package ru.papyrus.collector.servlet.form.helper;

import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

@Path("/checkContent")
@Slf4j
public class ContentVerifier {

    @GET
//    @Path("/title")
    @Produces(MediaType.APPLICATION_JSON)
    public Response verifyTitleXPath() {
        log.debug("Это verifyTitleXPath, ");

        return Response
                .status(Response.Status.OK)
                .encoding(StandardCharsets.UTF_8.name())
                .entity(new HashMap<>())
                .build();
    }
}
