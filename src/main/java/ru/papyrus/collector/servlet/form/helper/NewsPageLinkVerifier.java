package ru.papyrus.collector.servlet.form.helper;

import org.apache.commons.io.IOUtils;
import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.DomSerializer;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

@WebServlet(value = "/NewsCollector/checkNewsPageLink")
public class NewsPageLinkVerifier extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(NewsPageLinkVerifier.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//        LOGGER.debug("Это NewsPageLinkVerifier");
        String startPageURL = req.getParameter("startPage");
        String xPathString = req.getParameter("xPath");
        String encoding = req.getParameter("encoding");

        JSONObject ret = process(startPageURL, xPathString, encoding);

        resp.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = resp.getWriter()) {
            out.println(ret.toString());
        }
    }

    private JSONObject process(String startPageURL, String xPathString, String encoding) throws IOException {
//        LOGGER.debug("Это NewsPageLinkVerifier process");
        JSONObject ret = new JSONObject();

        /* Прогоняем по стартовой страничке полученный xPath */
        org.jsoup.nodes.Document doc = Jsoup.connect(startPageURL).get();
        String body = doc.body().outerHtml();
        HtmlCleaner cleaner = new HtmlCleaner();
        TagNode cleaned = cleaner.clean(IOUtils.toInputStream(body, encoding));
        try {
            Document document = new DomSerializer(new CleanerProperties()).createDOM(cleaned);
            XPath xpath = XPathFactory.newInstance().newXPath();
            NodeList newsPageNL = (NodeList) xpath.evaluate(xPathString, document, XPathConstants.NODESET);
//            LOGGER.debug("newsPageNL: {}", newsPageNL.getLength());
            if(newsPageNL.getLength() == 0) {
                ret.put("success", false);
                ret.put("message", "Не найдено ссылок на страницу новостей на " + startPageURL + ".");
                return ret;
            } else {
                URL url = new URL(startPageURL);
                String link = url.getProtocol() + "://" + url.getHost() + newsPageNL.item(0).getTextContent();
//                LOGGER.debug("link: " + link);
                /* Линк получен, теперь проверим, открывается ли он */
                HttpURLConnection conn = (HttpURLConnection) new URL(link).openConnection();
                if (HttpStatus.OK.value() == conn.getResponseCode()) {
                    ret.put("success", true);
                    return ret;
                } else {
                    ret.put("success", false);
                    ret.put("message", "Ошибка при получении страницы новостей " + startPageURL + ": " + HttpStatus.valueOf(conn.getResponseCode()).getReasonPhrase() + ".");
                    return ret;
                }
            }
        } catch (ParserConfigurationException e) {
            ret.put("success", false);
            ret.put("message", "Ошибка при парсинге стартовой страницы " + startPageURL + ".");
            return ret;
        } catch (XPathExpressionException e) {
            ret.put("success", false);
            ret.put("message", "Ошибка при применении xPath к стартовой странице " + startPageURL + ".");
            return ret;
        }
    }
}