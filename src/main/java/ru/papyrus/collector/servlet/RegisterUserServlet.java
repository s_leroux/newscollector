package ru.papyrus.collector.servlet;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.papyrus.collector.dao.SourceDAO;
import ru.papyrus.collector.dao.UsersDAO;
import ru.papyrus.collector.entity.Access;
import ru.papyrus.collector.entity.Source;
import ru.papyrus.collector.entity.User;
import ru.papyrus.collector.entity.UserSession;
import ru.papyrus.collector.main.App;
import ru.papyrus.collector.main.logger.DBLogger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;
import java.util.Date;

/**
 *
 */
@WebServlet(value = "/NewsCollector/registerUser")
public class RegisterUserServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegisterUserServlet.class);

    private DBLogger DB_LOGGER = App.getInstance().getDB_LOGGER();

    private UsersDAO usersDAO;
    private SourceDAO sourceDAO;

    @Override
    public void init() throws ServletException {
        super.init();
        this.usersDAO = App.getInstance().getUsersDAO();
        this.sourceDAO = App.getInstance().getSourceDAO();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//        LOGGER.debug("Это RegisterUserServlet doPost, req regData: " + req.getParameter("regData"));

        String decoded = new String(Base64.getDecoder().decode(req.getParameter("regData")));
        JSONObject json = new JSONObject(decoded);
//        LOGGER.debug("json: " + json);

        JSONObject ret = new JSONObject();

        /* Первым делом - проверить логин */
        String login = json.getString("un");
//        LOGGER.debug("login: " + login);
        User user = App.getInstance().getUsersDAO().getUserByLogin(login);
//        LOGGER.debug("user: " + user);

        /* Проверить, есть ли такой пользователь */
        if(user != null) {
            ret.put("success", false);
            ret.put("message", "Пользователь с таким логином уже существует.");
        } else {
            String familyName = new String(json.getString("fn").getBytes(), "UTF-8");
            String name = new String(json.getString("name").getBytes(), "UTF-8");

            user = new User(login, json.getString("pw"), familyName, name);

            /* Добавить доступ к источникам (подписки) */
            JSONArray subs = json.getJSONArray("subs");

            for (int i = 0; i < subs.length(); i++) {
                JSONObject sub = (JSONObject)subs.get(i);

                long sourceId = Long.parseLong(StringUtils.stripStart(sub.getString("id"), "subs_"));
                Source source = sourceDAO.getSourceById(sourceId);
                user.addAccess(new Access(source, user, true));
            }

            try {
                usersDAO.addUser(user);
                ret.put("success", true);
            } catch (Exception e) {
                ret.put("success", false);
                ret.put("message", "Ошибка при записи нового пользователя");

                LOGGER.error("Exception caught: " + e.getMessage());
                for (int i = 0; i < e.getStackTrace().length; i++) {
                    LOGGER.error(""+e.getStackTrace()[i]);
                }
            }
        }

        resp.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = resp.getWriter()) {
            out.println(ret.toString());
        }

        /* Надо как-то записать сессию. */
//        LOGGER.debug("Записываем сессию после регистрации, user FamilyName: " + user.getFamilyName());
        boolean success = App.getInstance().getSessionDAO().addSession(
            new UserSession(req.getSession().getId(), user, new Date())
        );
    }
}