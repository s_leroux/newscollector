package ru.papyrus.collector.servlet;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.papyrus.collector.dao.SourceDAO;
import ru.papyrus.collector.entity.Source;
import ru.papyrus.collector.main.App;
import ru.papyrus.collector.main.logger.DBLogger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

@WebServlet(value = "/NewsCollector/deleteSources")
public class DeleteSourcesServlet extends HttpServlet {
    private static Logger LOGGER = LoggerFactory.getLogger(DeleteSourcesServlet.class);

    private SourceDAO sourcesDAO;
    private DBLogger DB_LOGGER;

    @Override
    public void init() throws ServletException {
        super.init();

        this.sourcesDAO = App.getInstance().getSourceDAO();

        this.DB_LOGGER = App.getInstance().getDB_LOGGER();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        LOGGER.debug("Это DeleteSourcesServlet, req source: " + req.getParameter("source"));

        String sourceName = req.getParameter("source");

        Source source = sourcesDAO.getSourceByName(sourceName);

        boolean success = sourcesDAO.deleteSource(source) ;

        JSONObject ret = new JSONObject();
        if(success) {
            DB_LOGGER.info("Удалён источник " + source.getName() + ".");

            /* Перестартуем */
            App.getInstance().runFetchersTimer();
            ret.put("success", true);
        } else {
            ret.put("success", false);
            ret.put("message", "Не удалось удалить источники: ошибка на сервере.");
        }

        resp.setContentType("text/html;charset=UTF-8");

        PrintWriter out = resp.getWriter();
        try {
            out.println(ret.toString());
        } finally {
            out.close();
        }
    }
}
