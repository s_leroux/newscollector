package ru.papyrus.collector.servlet;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.papyrus.collector.dao.SessionDAO;
import ru.papyrus.collector.dao.SourceDAO;
import ru.papyrus.collector.dao.UsersDAO;
import ru.papyrus.collector.entity.Access;
import ru.papyrus.collector.entity.Source;
import ru.papyrus.collector.entity.User;
import ru.papyrus.collector.entity.UserSession;
import ru.papyrus.collector.main.App;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;
import java.util.List;

@WebServlet(value = "/NewsCollector/userProfile")
public class UserProfileServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserProfileServlet.class);

    private SessionDAO sessionDAO;
    private UsersDAO usersDAO;
    private SourceDAO sourceDAO;

    @Override
    public void init() throws ServletException {
        super.init();
        this.sessionDAO = App.getInstance().getSessionDAO();
        this.usersDAO = App.getInstance().getUsersDAO();
        this.sourceDAO = App.getInstance().getSourceDAO();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        LOGGER.debug("Это UserProfileServlet doPost");

        String decoded = new String(Base64.getDecoder().decode(req.getParameter("profileData")));
        JSONObject json = new JSONObject(decoded);
//        LOGGER.debug("UserProfileServlet json: " + json);

        JSONObject ret = new JSONObject();

        /* Проверить, действительно ли запрос пришёл от пользователя текущей сессии */
//        LOGGER.debug("Пользователь: " + json.getString("un") + ", фамилия: " + json.getString("fn") + ", имя: " + json.getString("pn"));
        UserSession userSession = sessionDAO.getSessionById(req.getSession().getId());
        if (userSession == null) {
            ret.put("success", false);
            ret.put("message", "Нет сессии пользователя.");
        } else {
            User user = userSession.getUser();
            if (user.getLogin().equals(json.getString("un"))) {
                /* Записать изменения */
                String name = new String(json.getString("pn").getBytes(), "UTF-8");
                user.setName(name);
                String familyName = new String(json.getString("fn").getBytes(), "UTF-8");
                user.setFamilyName(familyName);

                /* Удалить все и заново добавить нужные доступы */
                user.deleteAccesses();
                /* Добавить доступ к источникам (подписки) */
                JSONArray subs = json.getJSONArray("subs");

                for (int i = 0; i < subs.length(); i++) {
                    JSONObject sub = (JSONObject)subs.get(i);
                    long sourceId = Long.parseLong(StringUtils.stripStart(sub.getString("id"), "subs_"));
                    Source source = sourceDAO.getSourceById(sourceId);
                    user.addAccess(new Access(source, user, true));
                }

                /* Если есть новый пароль - поменять */
                try {
                    String pw = json.getString("pw");
                    /* Заменяем пароль */
                    user.setPassword(pw);
                } catch (JSONException e) {
                    /* Ничего не делаем */
                }

                try {
                    if (usersDAO.modifyUser(user)) {
                        ret.put("success", true);
                    } else {
                        ret.put("success", false);
                        ret.put("message", "Ошибка при записи нового пользователя.");
                    }
                } catch (Exception e) {
                    ret.put("success", false);
                    ret.put("message", "Ошибка при записи нового пользователя.");

                    LOGGER.error("Exception caught: " + e.getMessage());
                    for (int i = 0; i < e.getStackTrace().length; i++) {
                        LOGGER.error(""+e.getStackTrace()[i]);
                    }
                }
            } else {
                ret.put("success", false);
                ret.put("message", "Пользователь сессии не совпадает с заданным.");
            }
        }

        resp.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = resp.getWriter()) {
            out.println(ret.toString());
        }
    }
}
