/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.papyrus.collector.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpSession;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author roux
 */
@ServerEndpoint(value = "/endpoint", configurator = GetHttpSessionConfigurator.class)
public class NewsCollectorWSEndpoint {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    
//    private static final Set<Session> peers = Collections.synchronizedSet(new HashSet<>());

    public static ConcurrentHashMap<Session, String> getPeersMap() {
        return peersMap;
    }

    private static final ConcurrentHashMap<Session, String> peersMap = new ConcurrentHashMap<>();

//    private Session wsSession;
    private HttpSession httpSession;

//    public Set<Session> getPeers() {
//        return peers;
//    }
    
//    NewsCollectorWSEndpoint wsEndpoint = App.getInstance().getWebsocketEndpoint();
    
    /**
     * Конструктор.
     */
    public NewsCollectorWSEndpoint() {

    }
    
    @OnMessage
    public String onMessage(String message) {
        return null;
    }

    @OnOpen
    public void onOpen(Session peer, EndpointConfig config) {
//        LOGGER.debug("Это onOpen, peer: " + peer.getId());
        this.httpSession = (HttpSession) config.getUserProperties().get(HttpSession.class.getName());
//        this.wsSession = peer;

        LOGGER.debug("Присоединился новый пир, Id: " + peer.getId() + ", ip: " + peer.getUserProperties().get("javax.websocket.endpoint.remoteAddress") + ", session id: " + this.httpSession.getId());

        peersMap.put(peer, this.httpSession.getId());

//        peers.add(peer);
    }

    @OnClose
    public void onClose(Session peer) {
        LOGGER.debug("Пир отключился: " + peer.getId());
//        peers.remove(peer);
        peersMap.remove(peer);
    }
}
