package ru.papyrus.collector.main.logger;

/**
 *
 * @author roux
 */
public class Marker {
    private String markerText;

    public Marker(String margerText) {
        this.markerText = margerText;
    }

    public String getMarkerText() {
        return markerText;
    }

    public void setMarkerText(String markerText) {
        this.markerText = markerText;
    }
}
