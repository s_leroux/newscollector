package ru.papyrus.collector.main.logger;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import ru.papyrus.collector.dao.LogDAO;
import ru.papyrus.collector.dao.SessionDAO;
import ru.papyrus.collector.entity.LogLevels;
import ru.papyrus.collector.entity.LogRecord;
import ru.papyrus.collector.entity.UserPrivilege;
import ru.papyrus.collector.entity.UserSession;
import ru.papyrus.collector.main.App;
import ru.papyrus.collector.main.utils.Privileges;
import ru.papyrus.collector.websocket.NewsCollectorWSEndpoint;

import javax.transaction.Transactional;
import javax.websocket.Session;

/**
 *
 * @author roux
 * Логирование в базу данных. Причём с маркером.
 */
public class DBLogger {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    
    private static DBLogger instance = new DBLogger();

    public static DBLogger getInstance() {
        return instance;
    }
    
    @Autowired
    private LogDAO logDAO;

    @Autowired
    private SessionDAO sessionDAO;
    
    /**
     * Приватный конструктор
     */
    private DBLogger() {}
    
    /**
     * Основной метод. На него ссылаются все прочие.
     * @param marker
     * @param message
     * @param level 
     */
    public synchronized void log(Marker marker, String message, LogLevels level) {
//        LOGGER.debug("Это log, marker: " + marker + ", message: " + message + ", level: " + level);
        LogRecord newLogRecord;
        if(marker == null) {
            newLogRecord = new LogRecord(new Date(), level, "", message);
        } else {
            newLogRecord = new LogRecord(new Date(), level, marker.getMarkerText(), message);
        }

//        LOGGER.debug("newLogRecord: " + newLogRecord + ", logDAO: " + logDAO);
        logDAO.addLogRecord(newLogRecord);

        /* И отправить сообщение в веб-сокет */
        NewsCollectorWSEndpoint endPoint = App.getInstance().getWebsocketEndpoint();

        ConcurrentHashMap<Session, String> peersMap = endPoint.getPeersMap();
        for (Session peer : peersMap.keySet()) {
            /* Если сессии нет - не рассылать */
            String sessionId = peersMap.get(peer);
            UserSession session = sessionDAO.getSessionById(sessionId);
            if(session == null) {
                continue;
            } else {
                /* Если сессия есть - смотреть привилегии её пользователя */
                List<UserPrivilege> privileges = session.getUser().getUserPrivileges();
                for (UserPrivilege privilege: privileges) {
                    if (privilege.getPrivilege().name().equals(Privileges.LOG.name())) {
                        JSONObject logMessageObject = new JSONObject();
                        logMessageObject.put("logMessage", newLogRecord.toJSON());
                        try {
                            peer.getBasicRemote().sendText(logMessageObject.toString());
                        } catch (IOException e) {
                            LOGGER.error("Не удалось отправить сообщение в веб-сокет");
                        }
                        break;
                    }
                }
            }
        }
    }
    
    public void log(String message, LogLevels level) {
        this.log(null, message, level);
    }
    
    public void fatal(Marker marker, String messsage) {
        this.log(marker, messsage, LogLevels.FATAL);
    }
    public void fatal(String messsage) {
        this.log(null, messsage, LogLevels.FATAL);
    }
    
    public void error(Marker marker, String message) {
        this.log(marker, message, LogLevels.ERROR);
    }
    public void error(String message) {
        this.log(null, message, LogLevels.ERROR);
    }
    
    public void warn(Marker marker, String message) {
        this.log(marker, message, LogLevels.WARN);
    }
    public void warn(String message) {
        this.log(null, message, LogLevels.WARN);
    }
    
    public void info(Marker marker, String message) {
        this.log(marker, message, LogLevels.INFO);
    }
    public void info(String message) {
        this.log(null, message, LogLevels.INFO);
    }
    
    public void debug(Marker marker, String message) {
        this.log(marker, message, LogLevels.DEBUG);
    }
    public void debug(String message) {
        this.log(null, message, LogLevels.DEBUG);
    }
    
    public void trace(Marker marker, String message) {
        this.log(marker, message, LogLevels.TRACE);
    }
    public void trace(String message) {
        this.log(null, message, LogLevels.TRACE);
    }
}
