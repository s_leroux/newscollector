package ru.papyrus.collector.main.fetcher;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import javax.websocket.Session;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.commons.io.IOUtils;
import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.DomSerializer;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.json.JSONObject;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Node;

import org.w3c.dom.Document;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.NodeList;
import ru.papyrus.collector.dao.ArticleDAO;
import ru.papyrus.collector.dao.SessionDAO;
import ru.papyrus.collector.dao.SourceDAO;
import ru.papyrus.collector.entity.Article;
import ru.papyrus.collector.entity.Source;
import ru.papyrus.collector.entity.UserSession;
import ru.papyrus.collector.main.App;
import ru.papyrus.collector.main.logger.DBLogger;
import ru.papyrus.collector.main.utils.Util;
import ru.papyrus.collector.websocket.NewsCollectorWSEndpoint;

/**
 *
 * @author roux
 */
public class SimpleFetcher extends Fetcher{
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    
    private String encoding = null;
    private URL startPageUrl = null;
    private Boolean tracing = false;
    
    @Autowired
    private DBLogger DB_LOGGER;
    
    @Autowired 
    private ArticleDAO articleDAO;

    @Autowired
    private SessionDAO sessionDAO;
    
//    @Autowired
//    private LogDAO logDAO;
    
    @Autowired
    private SourceDAO sourceDAO;

    /**
     * Конструктор без параметров
     */
    public SimpleFetcher() {
//        DB_LOGGER.debug("Это пустой конструктор SimpleFetcher");
    }

    /**
//     * Конструктор с параметром
//     * @param source
//     */
//    public SimpleFetcher(Source source) {
////        DB_LOGGER.debug("Это конструктор SimpleFetcher, имя: " + source.getName() + ", startPage: " + source.getStartPage() + ", interval: " + source.getInterval());
//        this.source = source;
//        this.marker = new Marker(source.getName());
//    }

    @Override
    public void run() {
        try {
            this.tracing = new Boolean(source.getTracing());
        } catch (NullPointerException e) {
            /* Оставляем false */
        }

        if(tracing) {
            DB_LOGGER.debug(this.marker, "Запускается проход фетчера, поток: " + Thread.currentThread().getName());
        }

        /* Надо скачивать сайт по заданному значению StartPage */
        try {
            this.startPageUrl = new URL(source.getStartPage());
            if(tracing) {
                DB_LOGGER.debug(this.marker, "Стартовая страница: " + startPageUrl);
            }
            
            /* Просто распечатываем содержимое стартовой страницы */
//            URL startUrl = new URL(source.getStartPage());
//            String startPageContent = getContent(startUrl, startUrl.getProtocol());
            
            org.jsoup.nodes.Document doc = Jsoup.connect(source.getStartPage()).get();
            /* Ищем кодировку */
            for (Node headChild : doc.head().childNodes()) {
                if ("meta".equalsIgnoreCase(headChild.nodeName())
                        && !headChild.attr("http-equiv").isEmpty() && "Content-Type".equalsIgnoreCase(headChild.attr("http-equiv"))
                        && !headChild.attr("content").isEmpty()
                ) {
                    String contentAttr = headChild.attr("content");
                    encoding = contentAttr.substring(contentAttr.indexOf("charset=") + 8);
                    if(tracing) {
                        DB_LOGGER.debug(this.marker, "Найдена кодировка: " + encoding);
                    }
                    break;
                }
                if(!headChild.attr("charset").isEmpty()) {
                    encoding = headChild.attr("charset");
                    if(tracing) {
                        DB_LOGGER.debug(this.marker, "Найдена кодировка: " + encoding);
                    }
                    break;
                }
            }
            /* Если кодировка не найдена, ставим из базы */
            if(encoding == null) {
                encoding = source.getEncoding();
                DB_LOGGER.warn(this.marker, "Кодировка не найдена, используется из базы: " + encoding);
            }
            /* Если в базе также нет, ставим utf-8 */
            if(encoding == null) {
                encoding = "utf-8";
            }
            
            /* Далее - если указан xPath для ссылки на страницу с новостями, пытаемся её найти.
                Если нет - ищем новости здесь же. */
            NodeList newsNL;
            if (source.getNewsPageLink() != null && !source.getNewsPageLink().trim().isEmpty()) {
//                LOGGER.debug("Ищем ссылку на страницу новостей.");
                String body = doc.body().outerHtml();
                HtmlCleaner cleaner = new HtmlCleaner();
                TagNode cleaned = cleaner.clean(IOUtils.toInputStream(body, encoding));
                Document document = new DomSerializer(new CleanerProperties()).createDOM(cleaned);
//                DB_LOGGER.debug("Document:" + document.getDocumentElement().getTagName());
                XPath xpath = XPathFactory.newInstance().newXPath();
                NodeList newsPageNL = (NodeList) xpath.evaluate(source.getNewsPageLink(), document, XPathConstants.NODESET);
                if(newsPageNL.getLength() == 0) {
                    if(tracing) {
                        DB_LOGGER.error(this.marker, "По заданному xPath (" + source.getNewsPageLink() + ") ссылка на страницу новостей не найдена. Источник " + source.getName() + " не будет запускаться.");
                    }
                    SimpleFetcher.this.cancel();
                } else if(newsPageNL.getLength() > 1) {
                    if(tracing) {
                        DB_LOGGER.warn(this.marker, "По заданному xPath (" + source.getNewsPageLink() + ") найдено более одной ссылки. Будет применена первая из них (" + newsPageNL.item(0).getTextContent() + ").");
                    }                    
                }
                String link = startPageUrl.getProtocol() + "://" + startPageUrl.getHost() + newsPageNL.item(0).getTextContent();
//                LOGGER.debug("Найдена ссылка: " + link);
                newsNL = this.parseNewsListPage(link);
            } else {
//                DB_LOGGER.debug("Ищем новости на стартовой странице.");
                newsNL = this.parseNewsListPage(source.getStartPage());
            }
            
            /* Теперь последовательно (то есть в один поток) будем парсить все полученные страницы */
//            List<String> links = articleDAO.getLinksBySourceName(source.getName());
//            if(tracing) {
//                DB_LOGGER.debug("Источник " + source.getName() + "; найдено ссылок: " + links.size());
//            }
            

            /* TODO Надо упорядочить найденные статьи по времени */
            TreeSet<Article> foundArticlesSet = new TreeSet<>(new Comparator<Article>() {
                @Override
                public int compare(Article article1, Article article2) {
                    return article1.getArticleDate().compareTo(article2.getArticleDate());
                }
            });
            
//            DB_LOGGER.debug("На страничке " + source.getName() + " ссылок на статьи: " + newsNL.getLength());
            
            for (int i = 0; i < newsNL.getLength(); i++) {
                String linkPrefix = startPageUrl.getProtocol() + "://" + startPageUrl.getHost();
                String linkContent = newsNL.item(i).getTextContent();
                StringBuilder linkSB = new StringBuilder();
                if (!linkContent.startsWith(linkPrefix)) {
                    linkSB.append(linkPrefix);
                }
                linkSB.append(linkContent);

//                if (links.contains(link)) {
//                    /* Если эта ссылка уже есть в БД, пропускаем её */
//                    continue;
//                }
                long articlesFound = articleDAO.getArticlesCountByLink(linkSB.toString());
                if(articlesFound > 0) {
//                    DB_LOGGER.debug("Статью " + link + " пропускаем.");
                    continue;
                }
                /* Пробуем отправить лог в базу данных */
                DB_LOGGER.info(this.marker, "Найдена новая статья: " + linkSB.toString());
                
                Article article = this.parseNewsPage(linkSB.toString());
//                DB_LOGGER.debug("Источник: " + article.getSourceName() + ", Опубликована: " + article.getArticleDate() );
                if(article == null) {
                    DB_LOGGER.error(this.marker, "Не удалось получить статью " + linkSB.toString());
//                    break;
                    continue;
                }
                
                foundArticlesSet.add(article);
            }
            /* Отправляем полученные статьи в базу в упорядоченном виде */
            for(Article article: foundArticlesSet) {
//                LOGGER.debug("Статья " + article.getNewsLink() + ", источник " + article.getSourceName() + ", дата: " + article.getArticleDate());
                if(articleDAO.addArticle(article)) {
//                    LOGGER.debug("Статья сохранена");
                    NewsCollectorWSEndpoint endPoint = App.getInstance().getWebsocketEndpoint();
                    ConcurrentHashMap<Session, String> peersMap = endPoint.getPeersMap();
//                    LOGGER.debug("Peers: " + endPoint.getPeersMap().size());
                    for (Session peer : endPoint.getPeersMap().keySet()) {
                        String sessionId = peersMap.get(peer);
                        UserSession session = sessionDAO.getSessionById(sessionId);
                        Source articleSource = sourceDAO.getSourceByName(article.getSourceName());
                        if(false
                                || (session == null && articleSource != null && articleSource.getCommon())
                                || (session != null && session.getUser().getAccessSources(true).contains(source))
                        ) {
                            JSONObject newArticleObject = new JSONObject();
                            newArticleObject.put("newArticle", article.toJSON());
//                            LOGGER.debug("Отправляется новая статья для пира " + peer.getId() + ": " + article.getNewsTitle());
                            peer.getBasicRemote().sendText(newArticleObject.toString());
                        }
                    }
                } else {
                    DB_LOGGER.warn(this.marker, "Не удалось сохранить статью " + article.getNewsLink() + ".");
                }
            }
        } catch (MalformedURLException ex) {
            DB_LOGGER.error(this.marker, "MalformedURLException для источника " + source.getName() + ": " + source.getStartPage());
        } catch (IOException ex) {
            DB_LOGGER.error(this.marker, "Ошибка подключения к стартовой странице источника " + source.getName() + ", " + ex.getClass().getSimpleName() + ": " + ex.getMessage());
        } catch (ParserConfigurationException ex) {
            DB_LOGGER.error(this.marker, "ParserConfigurationException: " + ex.getMessage());
        } catch (XPathExpressionException ex) {
            DB_LOGGER.error(this.marker, "Не удалось применить xPath 1: " + ex.getMessage());
            for (int i = 0; i < ex.getStackTrace().length; i++) {
                DB_LOGGER.error("" + ex.getStackTrace()[i]);
            }
        } catch (Exception ex) {
            DB_LOGGER.error(this.marker, "Ошибка при выполнении обхода источника " + source.getName() + " " + ex.getClass().getSimpleName() + ": " + ex.getMessage());
            for (int i = 0; i < ex.getStackTrace().length; i++) {
                DB_LOGGER.error(this.marker, "" + ex.getStackTrace()[i]);
            }
        }
    }

    /**
     * Разбираем страницу с новостями.
     * Предполагается, что на странице есть несколько ссылок на новости. Именно их и будем выцарапывать.
     * @param link 
     */
    private NodeList parseNewsListPage(String link) {
//        DB_LOGGER.debug("Это parseNewsListPage, link: " + link);
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(link).get();
//            DB_LOGGER.debug("Страница новостей: " + doc.outerHtml());
            TagNode cleaned = new HtmlCleaner().clean(IOUtils.toInputStream(doc.body().outerHtml(), encoding));
            Document document = new DomSerializer(new CleanerProperties()).createDOM(cleaned);
            XPath xpath = XPathFactory.newInstance().newXPath();
            NodeList newsPageNL = (NodeList) xpath.evaluate(source.getNewsLink(), document, XPathConstants.NODESET);
            if(tracing) {
                DB_LOGGER.debug(this.marker, "NewsLink: " + source.getNewsLink());
                DB_LOGGER.debug(this.marker, "Всего найдено новостей на странице: " + newsPageNL.getLength());
            }
//            for (int i = 0; i < newsPageNL.getLength(); i++) {
//                DB_LOGGER.debug("Ссылка на новость: " +  (startPageUrl.getProtocol() + "://" + startPageUrl.getHost() + newsPageNL.item(i).getTextContent()) ) ;
//            }
            return newsPageNL;
        } catch (HttpStatusException ex) {
            DB_LOGGER.error(this.marker, "Ошибка при попытке получить страницу новостей " + link + " , " + ex.getClass().getSimpleName() + ": " +  ex.getMessage());
        } catch (SocketTimeoutException ex) {
            DB_LOGGER.error(this.marker, "Ошибка при попытке получить страницу новостей " + link + " , " + ex.getClass().getSimpleName() + ": " +  ex.getMessage());
        } catch (IOException ex) {
            DB_LOGGER.error(this.marker, "Ошибка при попытке получить страницу новостей " + link + " , " + ex.getClass().getSimpleName() + ": " +  ex.getMessage());
//            SimpleFetcher.this.cancel();
        } catch (ParserConfigurationException ex) {
            DB_LOGGER.error(this.marker, "ParserConfigurationException при попытке получить страницу новостей: " + ex.getMessage());
        } catch (XPathExpressionException ex) {
            DB_LOGGER.error(this.marker, "Не удалось применить xPath 2: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Разбираем на части страничку новости.
     * Возвращать будем объект-сущность для записи в БД
     * @param newsLink 
     */
    private Article parseNewsPage(String newsLink) {
        if (tracing) {
            DB_LOGGER.debug(this.marker, "Парсинг страницы: " + newsLink);
        }
//
        String newsTitle, newsContent;
        Date newsDate = null;
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(newsLink).get();
            
//            String body = doc.body().outerHtml();
//            if(tracing) {
//                DB_LOGGER.debug("encoding: " + encoding);
//                DB_LOGGER.debug("body: " + body);
//            }
            
//            TagNode cleaned = new HtmlCleaner().clean(IOUtils.toInputStream(doc.body().outerHtml(), encoding));
            TagNode cleaned = new HtmlCleaner().clean(doc.body().outerHtml());
            Document document = new DomSerializer(new CleanerProperties()).createDOM(cleaned);
            XPath xpath = XPathFactory.newInstance().newXPath();
            
            /* Вытаскиваем заголовок */
            if(tracing) {
                DB_LOGGER.trace(this.marker, "NewsTitle XPath: " + source.getNewsTitle());
            }
            if(source.getNewsTitle() == null ) {
                DB_LOGGER.error(this.marker, "Не задан NewsTitle XPath в источнике.");
                return null;
            }
            NodeList newsTitleNL = (NodeList) xpath.evaluate(source.getNewsTitle(), document, XPathConstants.NODESET);
            if (newsTitleNL.getLength() == 0) {
                DB_LOGGER.error(this.marker, "Не удалось найти заголовок для новости " + newsLink);
                newsTitle = "";
            } else {
                if (newsTitleNL.getLength() > 1) {
                    DB_LOGGER.warn(this.marker, "Найдено более одного заголовка для новости " + newsLink + "(" + newsTitleNL.getLength() + "). Будет использоваться только первый найденный.");
                }
                
                newsTitle = Util.normalizeString(newsTitleNL.item(0).getTextContent());
                
//                return null;
            }
            if(tracing) {
                DB_LOGGER.debug(this.marker, "Заголовок статьи " + newsLink + ": " + newsTitle);
            }
            
            /* Вытаскиваем дату */
            if (tracing) {
                DB_LOGGER.debug(this.marker, "NewsDate: " + source.getNewsDate());
            }
            NodeList newsDateNL = (NodeList) xpath.evaluate(source.getNewsDate(), document, XPathConstants.NODESET);
            if (newsDateNL.getLength() == 0) {
                DB_LOGGER.warn(this.marker, "Не удалось найти дату новости " + newsLink);
                /* Ставим текущую */
                newsDate = new Date();
            } else {
                if(newsDateNL.getLength() > 1) {
                    DB_LOGGER.warn(this.marker, "Найдено более 1 даты новости. Будет использоваться первая найденная.");
                }
                if(tracing) {
                    DB_LOGGER.trace(this.marker, "Формат даты: " + source.getNewsDateFormat());
                }
                
                SimpleDateFormat sdf = new SimpleDateFormat(source.getNewsDateFormat());
                try {
                    String foundDate = newsDateNL.item(0).getTextContent();
                    if (tracing) {
                        DB_LOGGER.debug(this.marker, "foundDate: " + foundDate);
                    }
                    newsDate = sdf.parse(foundDate);
                    if (tracing) {
                        DB_LOGGER.debug(marker, "Найдена дата новости: " + newsDate);
                    }
                } catch (ParseException ex) {
                    DB_LOGGER.error(this.marker, "Не удалось распознать дату для новости " + newsLink + ": " + newsDateNL.item(0).getTextContent() + ".");
                    newsDate = new Date();
                }
            }

            /* Вытаскиваем содержимое */
            if (tracing) {
                DB_LOGGER.debug(marker, "newsContent Xpath: " + source.getNewsContent());
            }
            NodeList newsContentNL = (NodeList) xpath.evaluate(source.getNewsContent(), document, XPathConstants.NODESET);
            if (newsContentNL.getLength() == 0) {
                DB_LOGGER.error(this.marker, "Не удалось найти содержимое новости " + newsLink);
                return null;
            } else {
                /* Берём все ноды и складываем текст в них */
                if (tracing) {
                    DB_LOGGER.debug(marker, "newsContentNL: " + newsContentNL.getLength());
                }
//
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < newsContentNL.getLength(); i++) {
//                    DB_LOGGER.debug("Content: " + Util.normalizeString(newsContentNL.item(i).getTextContent()));
                    sb.append(Util.normalizeString(newsContentNL.item(i).getTextContent())).append("\n");
                }
                newsContent = sb.toString().trim();
                if (tracing) {
                    DB_LOGGER.debug(marker, "Длина контента: " + newsContent.trim().length());
                }
            }
            
            /* Вытащили всё. Теперь надо создавать сущность. */
            return new Article(source.getName(), newsLink, newsTitle, newsContent, newsDate);
        } catch (IOException | ParserConfigurationException | XPathExpressionException ex) {
            DB_LOGGER.error(this.marker, "Ошибка при получении страницы с новостью " + ex.getClass().getSimpleName() + ": " + ex.getMessage());
        }
        return null;
    }
}
