/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.papyrus.collector.main.fetcher;

import java.util.TimerTask;
//import org.slf4j.LoggerFactory;
//import org.slf4j.Logger;
import ru.papyrus.collector.entity.Source;
import ru.papyrus.collector.main.logger.Marker;

/**
 *
 * @author roux
 */
public abstract class Fetcher extends TimerTask{
//    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    
    protected Source source;

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    protected Marker marker;

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }
    
//    public Fetcher(Source source, String sourcePath) {
//        this.sourcePath = sourcePath;
//        this.source = source;
//    }

    @Override
    public abstract void run();
    
}
