package ru.papyrus.collector.main;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;
import ru.papyrus.collector.dao.*;
import ru.papyrus.collector.entity.Source;
import ru.papyrus.collector.main.fetcher.Fetcher;
import ru.papyrus.collector.main.logger.DBLogger;
import ru.papyrus.collector.main.logger.Marker;
import ru.papyrus.collector.main.utils.AdminCreator;
import ru.papyrus.collector.websocket.NewsCollectorWSEndpoint;

import javax.websocket.Session;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

/**
 * Главный класс, где сосредотачивается основная запускающая логика
 * @author roux
 */
public class App {
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    /* Общая соль */
    public static final String COMMON_SALT = "583704b0-713e-4487-a12d-07fded0ca038";

    public static final SimpleDateFormat DISPLAY_DATE_FORMAT = new SimpleDateFormat("dd.MM.YYYY HH:mm:ss");

    private ScheduledExecutorService scheduler = null;
    
    private static App instance;

    public static App getInstance() {
        return instance;
    }

    @Autowired
    private VelocityEngineFactoryBean velocityEngineFactory;
    public VelocityEngineFactoryBean getVelocityEngineFactory() {
        return velocityEngineFactory;
    }

    @Autowired
    private AdminCreator adminCreator;

//    public LocalSessionFactoryBean getSessionFactory() {
//        return sessionFactory;
//    }
//
//    public void setSessionFactory(LocalSessionFactoryBean sessionFactory) {
//        this.sessionFactory = sessionFactory;
//    }

//    @Autowired
//    private LocalSessionFactoryBean sessionFactory;

    @Autowired
    private DBLogger DB_LOGGER;
    public DBLogger getDB_LOGGER() {
        return DB_LOGGER;
    }

    public NewsCollectorWSEndpoint getWebsocketEndpoint() {
        return websocketEndpoint;
    }

    @Autowired
    private NewsCollectorWSEndpoint websocketEndpoint;
    
    @Autowired 
    private SourceDAO sourceDAO;
    public SourceDAO getSourceDAO() {
        return sourceDAO;
    }
    
    @Autowired 
    private ArticleDAO articleDAO;
    public ArticleDAO getArticleDAO() {
        return articleDAO;
    }
    
    @Autowired
    private LogDAO logDAO;
    public LogDAO getLogDAO() {
        return logDAO;
    }

    @Autowired
    private UsersDAO usersDAO;
    public UsersDAO getUsersDAO() {
        return usersDAO;
    }

    @Autowired
    private SessionDAO sessionDAO;
    public SessionDAO getSessionDAO() {
        return sessionDAO;
    }

//    @Autowired
//    private LocalSessionFactoryBean sessionFactory;
    
//    private Timer fetchersTimer;
//
//    public void setFetchersTimer(Timer fetchersTimer) {
//        this.fetchersTimer = fetchersTimer;
//    }
//
//    public Timer getFetchersTimer() {
//        return fetchersTimer;
//    }

    public HashMap<String, Fetcher> getFetchersMap() {
        return fetchersMap;
    }

    private HashMap<String, Fetcher> fetchersMap = new HashMap<>(0);
    
    @Autowired
    private ApplicationContext appContext;
    
    /**
     * Конструктор без параметров
     */
    public App() {
        App.instance = this;
    }
    
    /**
     * Инициализация всего при запуске приложения.
     * В частности, здесь получаем список фетчеров из БД и проходимся по нему.
     */
    public void init() {
//        LOGGER.debug("Это init, sourceDAO: " + sourceDAO + ", adminCreator: " + adminCreator);
//        LOGGER.debug("appContext: " + appContext);
//        LOGGER.debug("sessionFactory: " + sessionFactory);

        /* Проверяем, есть ли в БД пользователь admin.
         * Если нет - создаём */
        if(usersDAO.getUserByLogin("admin") == null) {
            adminCreator.createAdmin();
        }

        /* Настраиваем очистку лога */
        /* Дата, когда должна произойти первая очистка - завтра */
        Calendar tom = Calendar.getInstance();
        tom.set(Calendar.MILLISECOND, 0);
        tom.set(Calendar.SECOND, 0);
        tom.set(Calendar.MINUTE, 0);
        tom.set(Calendar.HOUR_OF_DAY, 0);
        tom.add(Calendar.DAY_OF_YEAR, 1);
//        LOGGER.debug("Clean start: " + tom.getTime());
        new Timer().schedule(deleteObsoleteLogsTask, tom.getTime(), 1000*60*60*24);

        /* Настраиваем очистку старых сессий - проверяем каждый час */
        new Timer().schedule(deleteExpiredSessions, 0, 1000*60*60);

        /* Запускаем таймер фетчеров */
        DB_LOGGER.info("Запускается проход по источникам.");
        runFetchersTimer();

        LOGGER.info("Приложение запущено.");
    }

    /* Запускаем таймер фетчеров */
    public void runFetchersTimer() {

        Collection<Source> activeSources = sourceDAO.getActiveSources();
        DB_LOGGER.info("Всего найдено активных источников: " + activeSources.size());
        if (this.scheduler != null && !this.scheduler.isShutdown()) {
            this.scheduler.shutdown();
        }
        this.scheduler = Executors.newScheduledThreadPool(activeSources.size());

        activeSources.forEach((source) -> {
            Marker marker = new Marker(source.getName());
            try {
                Fetcher fetcher = (Fetcher)appContext.getBean(Class.forName(source.getSourceClass()));
                fetcher.setSource(source);
                fetcher.setMarker(new Marker(source.getName()));
                fetchersMap.put(source.getName(), fetcher);
                int interval = parseInterval(source.getInterval());
                scheduler.scheduleAtFixedRate(fetcher, 0, interval, TimeUnit.MINUTES);
                DB_LOGGER.info(marker, "Источник " + source.getName() + " запущен, интервал: " + interval);
            } catch (ClassNotFoundException e) {
                DB_LOGGER.error(marker, "Класс " + source.getSourceClass() + " не найден. Источник " + source.getName() + " не будет запущен в обработку.");
            } catch (ParseException e) {
                DB_LOGGER.error(marker, "Неверный интервал для источника: " + source.getInterval() + ". Этот источник не будет запущен в обработку.");
            } catch (Exception e) {
                DB_LOGGER.error(marker, "Ошибка при запуске таймера " + e.getClass().getSimpleName() + ": " + e.getMessage());
            }
        });

        /* Теперь в веб-сокет надо бросить уведомление о том, что переписаны источники */
//        LOGGER.debug("В веб-сокет бросается уведомление об источниках.");
        NewsCollectorWSEndpoint endPoint = App.this.getWebsocketEndpoint();
        ConcurrentHashMap<Session, String> peersMap = endPoint.getPeersMap();

        JSONObject sourcesRenewNotice = new JSONObject();
        sourcesRenewNotice.put("notice", "sourcesUpdated");

        for (Session peer : peersMap.keySet()) {
            try {
                peer.getBasicRemote().sendText(sourcesRenewNotice.toString());
            } catch (IOException e) {
                LOGGER.error("Не удалось отправить сообщение в веб-сокет");
            }
        }
    }
    
    /**
     * Парсим принятый в виде строки интервал
     * Строка должн быть в виде целого числа, за которым следует одна из букв:
     * m (минута), h(час), d(день), w(неделя), M(месяц), y(год).
     * Любые другие варианты не допускаются.
     * Возвращается количество минут.
     * @return
     * @throws ParseException 
     */
    private int parseInterval(String intervalString) throws ParseException {
        String regexString = "\\d+[mhdwMy]";
        if (intervalString.matches(regexString)) {
//            char lastChar = intervalString.charAt(intervalString.length()-1);
            int multiplier;
            switch(intervalString.charAt(intervalString.length()-1)) {
                case 'm': {
                    multiplier = 1;
                } break;
                case 'h': {
                    multiplier = 60;
                } break;
                case 'd': {
                    multiplier = 24*60;
                } break;
                case 'w': {
                    multiplier = 7*24*60;
                } break;
                case 'M': {
                    /* Будем считать, что в месяце 30 дней */
                    multiplier = 30*24*60;
                } break;
                case 'y': {
                    /* Будем считать, что в году 365 дней */
                    multiplier = 365*24*60;
                } break;
                default: {
                    multiplier = 1;
                }
            }
            return multiplier*Integer.parseInt(intervalString.substring(0, intervalString.length() - 1));
        } else {
            throw new ParseException("Unparseable interval: " + intervalString, 0);
        }
    }
    
    private final TimerTask deleteObsoleteLogsTask = new TimerTask() {
        @Override
        public void run() {
            /* Удалить старые логи */
            logDAO.deleteObsoleteLogs();
        }
    };

    private final TimerTask deleteExpiredSessions = new TimerTask() {
        @Override
        public void run() {
            /* Удалить старые сессии из базы */
            sessionDAO.deleteExpiredSessions();
        }
    };
}