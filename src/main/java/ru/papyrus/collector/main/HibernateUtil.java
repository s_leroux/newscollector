package ru.papyrus.collector.main;

import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.SessionFactory;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * Hibernate Utility class with a convenient method to get UserSession Factory
 * object.
 *
 * @author roux
 */
public class HibernateUtil {

    private static final SessionFactory SESSION_FACTORY;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(HibernateUtil.class);
    
    static {
        try {
            // Create the SessionFactory from standard (hibernate.cfg.xml) 
            // config file.
            SESSION_FACTORY = new AnnotationConfiguration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            // Log the exception. 
            LOGGER.error("Initial SessionFactory creation failed: " + ex.getClass() + ": " + ex.getMessage());
            for (int i = 0; i < ex.getStackTrace().length; i++) {
                LOGGER.error("" + ex.getStackTrace()[i]);
            }
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    public static SessionFactory getSessionFactory() {
        return SESSION_FACTORY;
    }
    
    public static void shutdown() { 
        // Close caches and connection pools 
        getSessionFactory().close(); 
    }
}
