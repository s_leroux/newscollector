package ru.papyrus.collector.main.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ru.papyrus.collector.dao.SourceDAO;
import ru.papyrus.collector.dao.UsersDAO;
import ru.papyrus.collector.entity.Access;
import ru.papyrus.collector.entity.Source;
import ru.papyrus.collector.entity.User;
import ru.papyrus.collector.entity.UserPrivilege;
import ru.papyrus.collector.main.logger.DBLogger;

import java.util.Collection;
//import ru.papyrus.collector.main.App;

/**
 * Вспомогательный класс. Единственно для того, чтобы проверить, присутствует ли в БД пользователь admin,
 * и если нет, то создать его.
 */
public class AdminCreator {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdminCreator.class);
    @Autowired
    private UsersDAO usersDAO;

    @Autowired
    private SourceDAO sourceDAO;

    @Autowired
    private DBLogger DB_LOGGER;

    /**
     * Дефолтный пустой конструктор
     */
    public AdminCreator() {

    }

    public void createAdmin() {
//        LOGGER.debug("Это createAdmin, userDAO: " + usersDAO);
        if(usersDAO.getUserByLogin("admin") == null) {
            User adminUser = new User("admin", "admin", "Админов", "Админ");

            /* Добавляем админу все, какие есть, привилегии */
            adminUser.addPrivilege(new UserPrivilege(adminUser, Privileges.ADMIN));
            adminUser.addPrivilege(new UserPrivilege(adminUser, Privileges.SOURCES));
            adminUser.addPrivilege(new UserPrivilege(adminUser, Privileges.LOG));
            adminUser.addPrivilege(new UserPrivilege(adminUser, Privileges.USERS));

            /* Добавляем админу в подписки все общие источники */
            Collection<Source> commonSources = sourceDAO.getCommonSources();
//            LOGGER.debug("Добавляем админу commonSources: " + commonSources.size());
            for (Source source: commonSources) {
                adminUser.addAccess(new Access(source, adminUser, true));
            }

            boolean success = usersDAO.addUser(adminUser);
            if(success) {
                LOGGER.info("Успешно создан пользователь admin.");
            } else {
                LOGGER.error("Не удалось создать пользователя admin.");
            }
        }
    }
}
