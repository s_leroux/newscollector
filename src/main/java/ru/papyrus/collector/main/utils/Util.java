/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.papyrus.collector.main.utils;

import ru.papyrus.collector.main.App;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

/**
 * Сборник статичных методов
 * @author roux
 */
public class Util {
    
    /* Преобразуем заданную строку к читабельному виду. тоесть удаляем вс\кие примочки из HTML */
    public static String normalizeString(String source) {
        return source
                .replace("&laquo;", "«")
                .replace("&raquo;", "»")
                .replace("&mdash;", "—")
                .replace("&ndash;", "–")
                .replace("&bdquo;", "„")
                .replace("&ldquo;", "“");
    }

    public static byte[] getPwdHash(String password, String personalSalt){
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update((App.COMMON_SALT + personalSalt).getBytes(StandardCharsets.UTF_8));
            return md.digest(password.getBytes(StandardCharsets.UTF_8));
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }
}