package ru.papyrus.collector.main.utils;

public enum Privileges {
    ADMIN,
    SOURCES,
    LOG,
    USERS,
}
