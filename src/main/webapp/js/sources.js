var deleteSourcesDialog;

/* Переменные для формы редактирования источника */
//var startPageContent = null;
var newsPageContent = null;

var intervalPattern = /^\d+[mhdwMy]?$/;

function renderSources() {
//    console.log("Это renderSources");

    fillSourcesSelect();

    /* Вешаем слушатель на комбо источников для экспорта */
    $("#sourcesSelect").on("change", function(evt) {
//        console.log("Это sourcesSelect change");
        var obj = [];
        $('#sourcesSelect option:selected').each(function(i) {
            obj.push($(this).val());
        });

        if(obj.length == 1 && $("#editSourceForm").length == 0) {
            $("#editSourceButton").removeClass("disabled");
        } else {
            $("#editSourceButton").addClass("disabled");
        }

        if(obj.length > 0 && $("#editSourceForm").length == 0) {
            $("#exportSourcesButton").removeClass("disabled");
        } else {
            $("#exportSourcesButton").addClass("disabled");
        }
    });

    /* Вешаем слушатель на кнопку "Экспорт" */
    $("#exportSourcesButton").on("click", function() {
        var obj = [];
        $('#sourcesSelect option:selected').each(function(i) {
            obj.push($(this).val());
        });
        if(obj.length > 0) {
            $.ajax({
                url: 'NewsCollector/getSources',
                type: "POST",
                data: {
                    names: JSON.stringify(obj),
                },
                success: function (data, textStatus, jqXHR) {
//                    console.log("data: " + data + ", textStatus: " + textStatus);
                    var a = document.createElement("a");
                    a.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(data));
                    a.setAttribute('download', "sources.json");
                    if (document.createEvent) {
                        var event = document.createEvent('MouseEvents');
                        event.initEvent('click', true, true);
                        a.dispatchEvent(event);
                    } else {
                        a.click();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Ошибка при получении источников для экспорта, textStatus: " + textStatus);
                }
            });
        }
    });

    /* Вешаем слушатель на файловый инпут импорта источников */
    var json;
    $("#sources_input").on("change", function(evt) {
//        console.log("Changed " + this.value);

        if($("#sources_input").val()) {
//            $("#importSourcesButton").removeClass("disabled");
            var reader = new FileReader();
            reader.readAsText(evt.target.files[0]);
        }
        reader.onload = function(evt) {
//            console.log("Это onLoad");
            try {
                json = JSON.parse(evt.target.result);
                $("#importSourcesButton").removeClass("disabled");
            } catch (e) {
                $("#importSourcesButton").addClass("disabled");
                showErrorDialog("Выбранный файл не является валидным json-файлом.");
            }
        };
        reader.onerror = function(evt) {
            showErrorDialog("Ошибка чтения файла " + evt.target.error.name);
        };
    });

    /* Вешаем слушатель на кнопку "Импорт" */
    $("#importSourcesButton").on('click', function(evt) {
        if($("#importSourcesButton").hasClass("disabled")) {
//            console.log("Disabled");
        } else {
//            var forceUpdate = $("#force_update_cb").prop("checked");
            var value = $("#sources_input").val();
            if(value) {
//                console.log("Кликнуто, Да, значение есть");
//                console.log("json: " + JSON.stringify(json));
                /* Отправляем загруженный json на сервер и ждём ответа. */
                $.ajax({
                    url: 'NewsCollector/importSources',
                    type: "POST",
                    data: {
                        forceUpdate: $("#force_update_cb").prop("checked"),
                        sources: JSON.stringify(json)
                    },
                    success: function (data, textStatus, jqXHR) {
//                        console.log("Получен ответ от сервера, data: " + data + ", textStatus: " + textStatus);
//                        console.log("json: " + json);

                        /* Вывести диалог */
                        var importMessageHTML = new EJS({url: 'templates/import-message_1.ejs'}).render({
                            data: JSON.parse(data)
                        });
//                        console.log("importMessageHTML: " + importMessageHTML);
                        var message = $(importMessageHTML);
                        message.dialog({
                            title: "Результат выполнения импорта",
                            closeOnEscape: true,
                            height: 260,
                            width: 380,
                            modal: true,
                            buttons: [
                                {
                                    text: "OK",
                                    click: function() {
                                        $( this ).dialog( "close" );
                                    }
                                }
                            ]
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Ошибка при имортировании источников, textStatus: " + textStatus);
                    }
                });
            } else {
//                console.log("Кликнуто, Нет, значения нет");
            }
        }
    });

    /* Вешаем слушатель на кнопку "Редактировать" */
    $("#editSourceButton").on('click', function(evt) {

        if($("#editSourceButton").hasClass("disabled")) {
//            console.log("Disabled");
        } else {
//            console.log("Enabled");
//            console.log("Это editSourceButton");
            /* TODO Обнулить переменные формы редактирования */
//            startPageContent = null;
            newsPageContent = null;

            /* Запросить на сервере html формы редактирования источника */
            var obj = [];
            $('#sourcesSelect option:selected').each(function(i) {
                obj.push($(this).val());
            });

            $.ajax({
                url: 'NewsCollector/editSourceForm',
                type: "POST",
                data: {"source": obj[0]},
                success: function (data, textStatus, jqXHR) {
//                    console.log("Форма диалога редактирования источника получена, data: " + data);
                    renderSourceEditForm(obj[0], data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Ошибка на сервере при получении формы редактирования источника.");
                }
            });
        }
    });
}

function renderSourceEditForm(sourceName, data) {
//    console.log("Это renderSourceEditForm, sourceName: " + sourceName + ", editSourceForm: " + $("#editSourceForm").length);

    if($("#editSourceForm").length > 0) {
        return;
    }

    /* Глушим кнопку "Редактировать" */
    $("#editSourceButton").addClass("disabled");

    /* Глушим комбобокс источников */
    $("#sourcesSelect")[0].sumo.disable();

    $(data).insertAfter($("#sourcesParamsTable")).hide().slideDown("fast");

    /* Сразу проверим корректность интервала */
    validateIntervalInput();
    $("input#intervalTF").on("change", function(evt) {
        validateIntervalInput();
    });

    /* Вешаем слушатель на кнопку удаления */
    $("#deleteSourcesButton").on('click', function() {
        var sourceName = $("#sourceNameInput").val();

        if(!deleteSourcesDialog) {
            $.ajax({
                url: 'NewsCollector/deleteSourcesForm',
                type: "POST",
                success: function (data, textStatus, jqXHR) {
//                    console.log("Форма диалога удаления источников получена 2, data: " + data);
                    deleteSourcesDialog = data;
                    showDeleteSourceForm();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Ошибка на сервере при получении формы удаления источников.");
                }
            });
        } else {
            showDeleteSourceForm();
        }
    });

    $("#newsLinkInput").on("change input", function(evt) {
        /* По любому изменению убираем результат и сообщение */
        $("#checkNewsLinkResult").removeClass("okResult").removeClass("errResult").removeClass("waitResult");
        $("#checkNewsLinkMessage").html("");
    });

    /* Вешаем слушатель на текстовое поле стартовой страницы - при любом изменении будем убирать картинку с результата. */
    /* TODO Потом ещё надо будет глушить последующие поля (?) */
    $("#startPageInput").on("change input", function(evt) {
        $("#checkStartPageResult").removeClass("okResult").removeClass("errResult").removeClass("waitResult");
        $("#checkNewsPageLinkButton").addClass("disabled");
        $("#checkNewsPageLinkResult").removeClass("waitResult").removeClass("okResult").removeClass("errResult");
        $("#checkNewsPageLinkMessage").html("");

        $("#checkNewsLinkButton").addClass("disabled");
        $("#checkNewsLinkResult").removeClass("waitResult").removeClass("okResult").removeClass("errResult");
        $("#checkNewsLinkMessage").html("");
        /* Обнулить переменные формы редактирования */
        startPageContent = null;
    });

    /* Вешаем слушатель на текстовое поле xPath ссылки на страницу новостей - при любом изменении будем убирать картинку с результатом и надпись */
    $("#newsPageLinkInput").on("change input", function(evt) {
        $("#checkNewsPageLinkResult").removeClass("okResult").removeClass("errResult").removeClass("waitResult");
        $("#checkNewsPageLinkMessage").html("");
        /* Если поле не пусто, то заглушить кнопку проверки новостной страницы и убрать всё с неё */
        if($("#newsPageLinkInput").val()) {
            $("#checkNewsLinkButton").addClass("disabled");
            $("#checkNewsLinkResult").removeClass("waitResult").removeClass("okResult").removeClass("errResult");
            $("#checkNewsLinkMessage").html("");
            /* Также открыть свою кнопку */
            $("#checkNewsPageLinkButton").removeClass("disabled");
        } else {
            $("#checkNewsLinkButton").removeClass("disabled");
            $("#checkNewsPageLinkButton").addClass("disabled");
        }
    });

    /* Вешаем слушатель на кнопку проверки стартовой страницы */
    $("#checkStartPageButton").on("click", function() {
//        console.log("Это checkStartPageButton: " + $("#startPageInput").val() + ", кодировка: " + $("#encodingTF").val());
        /* Заглушить последующие кнопки и убрать сообщения и индикаторы */
        $("#checkNewsPageLinkButton").addClass("disabled");
        $("#checkNewsPageLinkResult").removeClass("waitResult").removeClass("okResult").removeClass("errResult");
        $("#checkNewsPageLinkMessage").html("");

        $("#checkNewsLinkButton").addClass("disabled");
        $("#checkNewsLinkResult").removeClass("waitResult").removeClass("okResult").removeClass("errResult");
        $("#checkNewsLinkMessage").html("");

        $("#checkStartPageResult").addClass("waitResult");
        /* Пробуем получить стартовую страницу - отправляем URL на сервер */
        $.ajax({
            url: "NewsCollector/getStartPage",
            type: "POST",
            data: {
                "url": $("#startPageInput").val(),
                "enc": $("#encodingTF").val(),
            },
            success: function(data, textStatus, jqXHR) {
                var res = JSON.parse(data);

                if(res.success) {
                    $("#checkStartPageResult").removeClass("errResult").removeClass("waitResult").addClass("okResult");
                    /* Открыть кнопку проверки ссылки на страницу новостей */
                    if($("#newsPageLinkInput").val()) {
                        $("#checkNewsPageLinkButton").removeClass("disabled");
                    } else {
                        /* Открыть кнопку проверки поиска новостной статьи - если паттерна странички новостей нет */
                        $("#checkNewsLinkButton").removeClass("disabled");
                    }
                    /* Заменить кодировку */
                    if(res.encoding) {
                        $("#encodingTF").val(res.encoding);
                    }
                    /* Записать контент в переменную */
//                    startPageContent = res.content;
//                    console.log("startPageContent: " + startPageContent);
                    /* TODO Открыть дальнейшие поля (?) */
                } else {
                    $("#checkStartPageResult").removeClass("okResult").removeClass("waitResult").addClass("errResult");
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $("#checkStartPageResult").removeClass("okResult").removeClass("waitResult").addClass("errResult");
            }
        });
    });

    /* Вешаем слушатель на кнопку проверки ссылки на страницу новостей */
    $("#checkNewsPageLinkButton").on('click', function(evt) {
        if(!$("#checkNewsPageLinkButton").hasClass("disabled")) {
            /* Далее всё распадается на две половинки:
             1) проверить, что паттерн реально находится и
             2) найденная по паттерну ссылка реально открывается */

//             console.log("xPath: " + $("#newsPageLinkInput").val());
             var path = $("#newsPageLinkInput").val();

//            $("#checkNewsPageLinkResult").addClass("waitResult");
            /* Отправляем на checkNewsPageLink адрес стартовой страницы, кодировку и xPath */
            $.ajax({
                url: "NewsCollector/checkNewsPageLink",
                type: "POST",
                data: {
                    "startPage": $("#startPageInput").val(),
                    "xPath": $("#newsPageLinkInput").val(),
                    "encoding": $("#encodingTF").val(),
                },
                success: function(data, textStatus, jqXHR) {
                    var res = JSON.parse(data);
                    if(res.success) {
                        $("#checkNewsPageLinkResult").removeClass("waitResult").removeClass("errResult").addClass("okResult");
                        $("#checkNewsPageLinkMessage").html("");
                        /* Открыть кнопку проверки новостной статьи */
                        $("#checkNewsLinkButton").removeClass("disabled");
                    } else {
                        $("#checkNewsPageLinkResult").removeClass("waitResult").removeClass("okResult").addClass("errResult");
                        $("#checkNewsPageLinkMessage").html(res.message);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#checkNewsPageLinkResult").removeClass("waitResult").removeClass("okResult").addClass("errResult");
                    $("#checkNewsPageLinkMessage").html("Ошибка на сервере.");
                }
            });
        }
    });

    /*  Вешаем слушатель на кнопку проверки поиска новостной статьи */
    $("#checkNewsLinkButton").on('click', function(evt) {
        if(!$("#checkNewsLinkButton").hasClass("disabled")) {
//            console.log("Это checkNewsLinkButton");
            $("#checkNewsLinkResult").addClass("waitResult")
            $.ajax({
                url: "NewsCollector/checkNewsLink",
                type: "POST",
                data: {
                    "startPage": $("#startPageInput").val(),
                    "encoding": $("#encodingTF").val(),
                    "newsPageLinkXPath": $("#newsPageLinkInput").val(),
                    "newsLinkXPath": $("#newsLinkInput").val(),
                },
                success: function(data, textStatus, jqXHR) {
                    console.log("Data: " + data);
                    var res = JSON.parse(data);

                    if(res.success) {
                        $("#checkNewsLinkResult").removeClass("waitResult").removeClass("errResult").addClass("okResult");
                        $("#checkNewsLinkMessage").html("");

                        newsPageContent = res.content;

                        /* Открыть кнопки проверки заголовка, тела и даты статьи */
                        $("#checkNewsTitleButton").removeClass("disabled");
                        $("#checkNewsBodyButton").removeClass("disabled");
                        $("#checkNewsDateButton").removeClass("disabled");
                    } else {
                        $("#checkNewsLinkResult").removeClass("waitResult").removeClass("okResult").addClass("errResult");
                        $("#checkNewsLinkMessage").html(res.message);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#checkNewsLinkResult").removeClass("waitResult").removeClass("okResult").addClass("errResult");
                    $("#checkNewsLinkMessage").html("Ошибка на сервере.");
                }
            });
        }
    });

    /* Вешаем слушатель на текстовое поле XPath поиска новостной статьи - будем глушить кнопки проверки заголовка, тела, даты и формата даты */
    $("#newsLinkInput").on("change input", function(evt) {
        $("#checkNewsTitleButton").addClass("disabled");
        $("#checkNewsTitleResult").removeClass("okResult").removeClass("errResult").removeClass("waitResult");
        $("#checkNewsTitleMessage").html("");

        $("#checkNewsBodyButton").addClass("disabled");
        $("#checkNewsBodyResult").removeClass("okResult").removeClass("errResult").removeClass("waitResult");
        $("#checkNewsBodyMessage").html("");

        $("#checkNewsDateButton").addClass("disabled");
        $("#checkNewsDateResult").removeClass("okResult").removeClass("errResult").removeClass("waitResult");
        $("#checkNewsDateMessage").html("");

        $("#checkDateFormatButton").addClass("disabled");
        $("#checkDateFormatResult").removeClass("okResult").removeClass("errResult").removeClass("waitResult");
        $("#checkDateFormatMessage").html("");
    });

    /* TODO Вешаем слушатель на кнопку проверки заголовка */
    $("#checkNewsTitleButton").on("click", function() {
        console.log("Это checkNewsTitleButton");
        /* Отправить на сервер значение поля newsTitleInput и текущее значение newsPageContent */
        $.ajax({
            url: '/NewsCollector/checkContent',
                type: "POST",
                data: {
                    xpath: $("#newsTitleInput").val(),
                    content: newsPageContent,
                },
                success: function (data, textStatus, jqXHR) {
                    console.log("Success, data: " + data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error, errorThrown: " + errorThrown);
                },
        });
    });

    /* Вешаем слушатель на кнопку отмены */
    $("#cancelEditButton").on('click', function(evt) {
        $("#editSourceForm").slideUp("slow").hide().remove();
        /* И открываем кнопку "Редактировать" */
        if($("#editSourceForm").length == 0) {
            $("#editSourceButton").removeClass("disabled");
        }
        /* И открыть комбобокс источников */
        $("#sourcesSelect")[0].sumo.enable();
    });

    /* TODO Вешаем слушатель на кнопку OK */
    $("#okEditButton").on('click', function(evt) {
        console.log("Это okEditButton");
    });
}

function fillSourcesSelect() {
    console.log("Это fillSourcesSelect, sourcesJSON: " + JSON.stringify(sourcesJSON));

    var sourcesSelectHTML = new EJS({url: 'templates/sourcesselect.ejs'}).render({
        data: sourcesJSON
    });
    console.log("sourcesSelectHTML: " + sourcesSelectHTML);

    $("#sourcesSelect").html(sourcesSelectHTML);

    $("#sourcesSelect").SumoSelect({
        okCancelInMulti:true,
        selectAll:true
    });
    $("#sourcesSelect")[0].sumo.reload();

    $("#sourcesSelect").trigger("change");
}

function showDeleteSourceForm() {
//    console.log("Это showDeleteSourceForm");

    /* Если формы нет, создаём */
    if (!w2ui.deleteSourcesForm) {
//        console.log("Создаём форму удаления источников.");
        $().w2form({
            name: 'deleteSourcesForm',
            style: 'border: 0px; background-color: transparent;',
            formHTML: deleteSourcesDialog,
            actions: {
                "okDelete": function () {
                    $.ajax({
                        url: 'NewsCollector/deleteSources',
                        type: "POST",
                        data: {
                            source: $("#sourceNameInput").val(),
                        },
                        success: function (data, textStatus, jqXHR) {
                            var dataJSON = JSON.parse(data);
                            if(dataJSON.success) {
                                if(dataJSON.success && JSON.stringify(dataJSON.success) === 'true') {
                                    /* Закрываем диалог */
                                    w2popup.close();
                                    /* Закрываем форму редактирования */
                                    $("#cancelEditButton").trigger('click');
                                }
                            } else if(JSON.stringify(dataJSON.success) === 'false' && dataJSON.message) {
                                $("#deleteSources-result").html(dataJSON.message);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $("#deleteSources-result").html("Ошибка на сервере при удалении источников.");
                            console.log("Ошибка на сервере при удалении источников.");
                        }
                    });
                },
                "cancelDelete": function () {
                    w2popup.close();
                }
            }
        });
    }

    $().w2popup('open', {
        title   : 'Удаление',
        body    : '<div id="removeSourcesForm" style="width: 100%; height: 100%;"></div>',
        style   : 'padding: 15px 0px 0px 0px',
        width   : 350,
        height  : 180,
        speed: 0,
        showMax : false,
        onOpen: function (event) {
            event.onComplete = function () {
                $('#w2ui-popup #removeSourcesForm').w2render('deleteSourcesForm');

                $("#source2delete").html($("#sourceNameInput").val());

                /* Переносим фокус на кнопку Отмена */
                $("#cancelDelete").focus();
            }
        }
    });
}

function validateIntervalInput() {
//    var interv = $("input#intervalTF").val();
    if(intervalPattern.test($("input#intervalTF").val())) {
        $("input#intervalTF").removeClass("incorrect");
    } else {
        /* Подкрасить поле */
        $("input#intervalTF").addClass("incorrect");
    }
}