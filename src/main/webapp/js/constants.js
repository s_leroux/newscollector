var constants = {
    NOT_LOGGED_IN: "Вход не выполнен",
    LOG_IN: "Вход",
    LOG_OUT: "Выйти",
    AUTH_ERROR: "Ошибка при проверке авторизации.",
    INVALID_LOGIN_PASSWORD: "Неверное имя пользователя или пароль.",
    EMPTY_LOGIN_OR_PASSWORD: "Пустой логин или пароль.",
    LOGIN_OR_PW_CONTAINING_SPACE: "Логин или пароль содержат пробел.",
    STRING_CONTAINS_INVALID_CHARACTER: "Некорректные символы",
    UNRECOGNIZED_ERROR: "Произошла неожиданная ошибка"
}


