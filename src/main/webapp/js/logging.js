var levels = ["trace", "debug", "info", "warn", "error", "fatal"];

function onScriptLoad() {
    /* Запрашиваем сервер о сегодняшних логах */
    $.ajax({
    url: 'NewsCollector/getLog',
    type: "POST",
    data: {
        term: (new Date()).toDateString(),
    },
    success: function (data, textStatus, jqXHR) {
    //            console.log("Лог получен, data: " + data);
        fillLog(data);
    },
    error: function (jqXHR, textStatus, errorThrown) {
        console.log("Ошибка при получении сегодняшнего лога, textStatus: " + textStatus);
    }
    });

    /* Поле даты лога */
    $("#logDatePicker").datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        onSelect: function() {
            getLogByDate($(this).datepicker('getDate'));
        }
    }).datepicker("setDate", new Date());

    /* Вешаем слушатель на селектор уровня логирования */
    $("#logLevelSelect").on("change", function() {
        logLevelChanged(this.value);
    });

    /* Вешаем слушатель на селектор источников */
    $("#logSourceSelect").on("change", function() {
        filterLogSource(this.value);
    });

    /* Вешааем слушатель на кнопку скачивания лога */
    $("#downloadLogButton").on("click", function() {
        downloadLog();
    });
}

/* Скачиваем лог */
function downloadLog() {
    var arr = "";
    $($("#log_area").find("table").find("tbody").find("tr").get().reverse()).each(function() {
        var outString = $(this).find("span.log-date").text() + " - [" + $(this).find("span.log-source").text() + " - " + $(this).attr("class").split("_")[1].toUpperCase() + "] - " + $(this).find("span.log-text").text() + "\n";
        arr += outString;
    });

    /* Имя файла - дата, уровень, источник */
    var date = new Date($("#logDatePicker").datepicker("getDate").getTime());

//        var filename = (date.getDate().pad() + "." + (date.getMonth()+1).pad() + "." + date.getFullYear()) + "_" + $("#logSourceSelect").val() + "_ " + levels[$("#logLevelSelect").val()] + ".log";

    var a = document.createElement("a");
    a.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(arr));
    a.setAttribute('download', (date.getDate().pad() + "." + (date.getMonth()+1).pad() + "." + date.getFullYear()) + "_" + $("#logSourceSelect").val() + "_ " + levels[$("#logLevelSelect").val()] + ".log");
    if (document.createEvent) {
        var event = document.createEvent('MouseEvents');
        event.initEvent('click', true, true);
        a.dispatchEvent(event);
    } else {
        a.click();
    }
}



function fillLog(data) {
    var logObject = JSON.parse(data);

//    for(var i = 0; i < logObject.length; i++) {
//        console.log("Message " + i + " text: " + logObject[i].text + ", source:" + logObject[i].source);
//    }
    
    var logHTML = new EJS({url: 'templates/logging.ejs'}).render({
        data: logObject,
    });
    
    $("#log_area").html(logHTML);

    /* Также из объекта лога вытащить все уникальные значения источников (кроме пустого) */
    /* Добавлять только тогда, когда таких ещё в комбобоксе нет */
    var uniqueSources = getUniqueSources(logObject);
    for(var i= 0; i < uniqueSources.length; i++) {
//        console.log("uniqueSources " + i + ": " + uniqueSources[i]);
        if($("#logSourceSelect option[value='" + uniqueSources[i] + "']").length == 0) {
            $("#logSourceSelect").append("<option value=\"" + uniqueSources[i] + "\">" + uniqueSources[i] + "</option>");
        }
    }
}

function addLogMessage(data) {
//    console.log("Это addLogMessage, logLevelSelect: " + $("#logLevelSelect").val() + ", source: " + $("#logSourceSelect").val());
//    console.log("Это addLogMessage, logLevelSelect: " + $("#logLevelSelect").val() + ", в данных: " + data.level);
    /* Поставить ограничение - добавлять сообщение только тогда, когда выбрана текущая дата. */
    var today = (new Date());
    today.setHours(0,0,0,0);
    if(today.getTime() != $("#logDatePicker").datepicker("getDate").getTime()) {
        return;
    }
    /* Собственно добавляем запись лога */
    try {
        var levelAdd = getLevelVisibility(data.level, $("#logLevelSelect").val())?"":" log_level_invisible";
        var sourceAdd = getSourceVisibility(data.source, $("#logSourceSelect").val())?" log_source_invisible":"";
        var toAdd = "<tr class=\"record_" + data.level + levelAdd + sourceAdd + "\" source=\"" + data.source + "\"><td><span class=\"log-date\">" + data.recordDate + "</span></td><td><span class=\"log-source\">" + data.source + "</span></td> <td><span class=\"log-text\">" + data.text + "</span></td></tr>";
        $("#log_area").find("table").find("tbody").prepend(toAdd);
    } catch (e) {
        console.log("Не удалось вставить сообщение в лог: " + e);
    }

    /* Также проверить, есть ли такой источник в комбобоксе. Если нет - добавить */
    var existingSourceOptions = [];
    $("#logSourceSelect option").each(function() {
        existingSourceOptions.push($(this).val());
    });
    if(!existingSourceOptions.includes(data.source)) {
        $('#logSourceSelect').append($("<option></option>").attr("value",data.source).text(data.source));
    }
}

function getSourceVisibility(messageSource, currentSource) {
//    console.log("Это getSourceVisibility, messageSource: " + messageSource + ", currentSource: |" + currentSource + "|");
    if(currentSource === "all") {
        return false;
    } else if(messageSource == "" && currentSource == "no_source") {
        return false;
    } else if(messageSource == currentSource) {
        return false;
    }
    return true;
}

function getLevelVisibility(messageLevel, currentLevel) {
//    console.log("Это getLevelVisibility, messageLevel: " + messageLevel + ", currentLevel: " + currentLevel);
    /* Нужно выдавать либо log_record_visible, либо log_record_invisible
        Для fatal выдавать всегда log_record_visible */
    switch(messageLevel) {
        case 'trace': {
            return currentLevel <= 0;
        } break;
        case 'debug': {
            return currentLevel <= 1;
        } break;
        case 'info': {
            return currentLevel <= 2;
        } break;
        case 'warn': {
            return currentLevel <= 3;
        } break;
        case 'error': {
            return currentLevel <= 4;
        } break;
        case 'fatal': {
            return true;
        } break;
    }
}

function logLevelChanged(value) {
    if(value) {
//        console.log("logLevelChanged, value: " + value);
        for(var i = 0; i < levels.length; i++) {
            if(i >= value) {
                $("tr.record_" + levels[i]).removeClass("log_level_invisible");
            } else {
                $("tr.record_" + levels[i]).addClass("log_level_invisible");
            }
        }
    }
}

function getUniqueSources(object) {
    var arr = [];
    for(var i = 0; i < object.length; i++) {
        if(object[i].source && !arr.includes(object[i].source)) {
            arr.push(object[i].source);
        }
    }
    return arr;
}

function filterLogSource(value) {
    if(value) {
        if("all" == value) {
            $("tr[class^='record_']").removeClass("log_source_invisible");
        } else if("no_source" == value) {
            $("tr[class^='record_'][source!='']").addClass("log_source_invisible");
            $("tr[class^='record_'][source='']").removeClass("log_source_invisible");
        } else {
            $("tr[class^='record_'][source!='" + value + "']").addClass("log_source_invisible");
            $("tr[class^='record_'][source='" + value + "']").removeClass("log_source_invisible");
        }
    }
}

function getLogByDate(date) {
//    console.log("Это getLogByDate, date: " + date);
    $.ajax({
        url: 'NewsCollector/getLog',
        type: "POST",
        data: {
            term: date.toDateString(),
        },
        success: function (data, textStatus, jqXHR) {
//            console.log("Лог по дате получен, data: " + data);
            fillLog(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Ошибка при получении лога, textStatus: " + textStatus);
        }
    });
}