var dataJSON;

function initSearching() {
    /* Пикер "от" */
    $("#searchDateFromPicker").datetimepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        controlType: 'select',
        oneLine: true,
        showButtonPanel: false,
        timeText: 'Время',
        currentText: 'Сейчас',
        closeText: 'Закрыть'
    });

    /* Пикер "до" */
    $("#searchDateToPicker").datetimepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        controlType: 'select',
        oneLine: true,
        showButtonPanel: false,
        timeText: 'Время',
        currentText: 'Сейчас',
        closeText: 'Закрыть'
    });
}

function onClearButtonClick() {
    /* Очищаем все поля ввода */
//    console.log("Это onClearButtonClick");
    for(var i = 0; i < $('#searchSourcesSelect option').length; i++) {
        $("#searchSourcesSelect")[0].sumo.unSelectItem(i);
    }
    $("#searchDateFromPicker").val("");
    $("#searchDateToPicker").val("");
    $("#titleContext").val("");
    $("#bodyContext").val("");

    $("#search_summary").parent().addClass("summary_hidden");
    $("#search_results").html("");
    if($("#search_results").hasClass("ui-accordion")) {
        $("#search_results").accordion("refresh");
    }
}

//function onSearchFieldChange(value) {
////    console.log("Это onSearchFieldChange, value: " + value.length);
//    if(value.length > 0) {
//        /* Активируем кнопку поиска */
//        $("#submitSearchButton").removeClass("disabled");
//    } else {
//        /* Проверяем все поля. Если все пусты - глушим кнопку поиска. Если не все - активируем */
//        if(true
//            && $("#searchSourcesSelect").val().length == 0
//            && $("#searchDateFromPicker").val().length == 0
//            && $("#searchDateToPicker").val().length == 0
//            && $("#titleContext").val().length == 0
//            && $("#bodyContext").val().length == 0
//        ) {
//            $("#submitSearchButton").addClass("disabled");
//        } else {
//            $("#submitSearchButton").removeClass("disabled");
//        }
//    }
//}

/* Надо собрать все данные с формы и отправить на сервер */
function submitSearchForm() {
//    console.log("Это submitSearchForm");

    var sourcesValue = $("#searchSourcesSelect").val();
    var dateFromValue = $("#searchDateFromPicker").val()?$("#searchDateFromPicker").datepicker("getDate").getTime():"";
    var dateToValue = $("#searchDateToPicker").val()?$("#searchDateToPicker").datepicker("getDate").getTime():"";
    var titleContext = $("#titleContext").val();
    var bodyContext = $("#bodyContext").val();

    if((sourcesValue + dateFromValue + dateToValue + titleContext + bodyContext).trim() === "") {
        return;
    }

    /* Очищаем результаты */
    $("#search_results").html("");
    if($("#search_results").hasClass("ui-accordion")) {
        $("#search_results").accordion("refresh");
    }
    $("#search_summary").parent().addClass("summary_hidden");

    /* Показываем крутилку */
    $("#search_waiter").addClass("busy_indicator");

    $.ajax({
        url: 'NewsCollector/getArticles',
        type: "POST",
        data: {
            sources: sourcesValue,
            dateFrom: dateFromValue,
            dateTo: dateToValue,
            titleContext: titleContext,
            bodyContext: bodyContext,
        },
        success: function (data, textStatus, jqXHR) {
//            console.log("data: " + data);
            dataJSON = JSON.parse(data);

            if(dataJSON.count == 0) {
                $("#search_summary").html("Ничего не найдено.");
                $("#download-select").attr("disabled", "disabled");
            } else {
                $("#download-select").removeAttr("disabled");
                if(dataJSON.count > dataJSON.articles.length) {
                    /* Это будут показаны не все */
                   $("#search_summary").html("Всего найдено статей: " + dataJSON.count + ". Из них показано последние " + dataJSON.articles.length + ".");
                } else {
                    $("#search_summary").html("Найдено статей: " + dataJSON.articles.length + ".");
                }
            }

            /* Теперь полученные данные надо отобразить */
            var articlesHTML = new EJS({url: 'templates/articles.ejs'}).render({
                data: dataJSON.articles
            });
//            console.log("articlesHTML: " + articlesHTML);
            $("#search_results").html(articlesHTML);

            if($("#search_results").hasClass("ui-accordion")) {
                $("#search_results").accordion("refresh");
            } else {
                $("#search_results").accordion({
                    active:false,
                    collapsible: true,
                    heightStyle: "content"
                });
            }

            $("#search_waiter").removeClass("busy_indicator");
            $("#search_summary").parent().removeClass("summary_hidden");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Ошибка при получении статей по параметрам поиска, textStatus: " + textStatus);
            $("#search_waiter").removeClass("busy_indicator");
        }
    });
}

function downloadFoundArticles(format) {
//    console.log("Это downloadFoundArticles, format: " + format);
    var resString = '', fileName = '';
    switch(format) {
        case 'Текст': {
            resString = OBJ2Text(dataJSON);
            fileName = 'results.txt';
        } break;
        case 'XML': {
            resString = formatXml(OBJtoXML(dataJSON));
            fileName = 'results.xml';
        } break;
        case 'Json': {
            resString = JSON.stringify(dataJSON, null, "\t");
            fileName = 'results.json';
        }
    }

    if(resString.length > 0) {
        var a = document.createElement("a");
        a.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(resString));
        a.setAttribute('download', fileName);
        if (document.createEvent) {
            var event = document.createEvent('MouseEvents');
            event.initEvent('click', true, true);
            a.dispatchEvent(event);
        } else {
            a.click();
        }
    }
}

function OBJtoXML(obj) {
    var xml = '';
    for (var prop in obj) {
        xml += obj[prop] instanceof Array ? '' : "<" + prop + ">";
        if (obj[prop] instanceof Array) {
            for (var array in obj[prop]) {
                xml += "<" + prop + ">";
                xml += OBJtoXML(new Object(obj[prop][array]));
                xml += "</" + prop + ">";
            }
        } else if (typeof obj[prop] == "object") {
            xml += OBJtoXML(new Object(obj[prop]));
        } else {
            xml += obj[prop];
        }
        xml += obj[prop] instanceof Array ? '' : "</" + prop + ">";
    }
    var xml = xml.replace(/<\/?[0-9]{1,}>/g, '');
    return xml;
}

function formatXml(xml, tab) {
    var formatted = '', indent= '';
    tab = tab || '\t';
    xml.split(/>\s*</).forEach(function(node) {
        if (node.match( /^\/\w/ )) indent = indent.substring(tab.length); // decrease indent by one 'tab'
        formatted += indent + '<' + node + '>\r\n';
        if (node.match( /^<?\w[^>]*[^\/]$/ )) indent += tab;              // increase indent
    });
    return formatted.substring(1, formatted.length-3);
}

function OBJ2Text(obj) {
    var ret = "";

    for (var no in dataJSON.articles) {
        if(ret.length > 0) {ret += "\n\n";}
        var item = dataJSON.articles[no];
        ret += item.articleDate + " - " + item.newsTitle.toUpperCase() + " (" + item.sourceName + ")\n";
        ret += item.content;
    }
    return ret;
}

function fillSearchSelect() {
//    console.log("Это fillSearchSelect, sourcesJSON: " + JSON.stringify(sourcesJSON));
    var sourcesSelectHTML = new EJS({url: 'templates/sourcesselect.ejs'}).render({
        data: sourcesJSON
    });

    console.log("sourcesSelectHTML: " + sourcesSelectHTML);

    $("#searchSourcesSelect").html(sourcesSelectHTML);
    $("#searchSourcesSelect").SumoSelect({
        okCancelInMulti:true,
        selectAll:true
    });

    $("#searchSourcesSelect")[0].sumo.reload();
}