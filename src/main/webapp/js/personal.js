/* Запрашиваем с сервера кнопки меню - в зависимости от хранящейся в кукисах информации о сессии. */
function requestForMenuButtons() {
//    console.log("Это requestForMenuButtons");
    $.ajax({
        url: 'NewsCollector/getMenuButtons',
        type: "POST",
//        data: {
//            sessionId: "c0b8893c-800e-4377-84eb-6b575d103890",
//        },
        success: function (data, textStatus, jqXHR) {
//            console.log("Это success, data: " + data);
            $(".button.menu-button.auth-button").remove();
            var arr = JSON.parse(data);
            for(var i = arr.length-1; i >= 0; i--) {
                var style
                var b = $("<div>")
                    .attr("id", arr[i].id)
                    .attr("class", "button menu-button auth-button")
                    .attr("title", arr[i].title)
                    .attr("onclick", arr[i].onclick)
                    .attr("style", "background-image: url(data:image/svg+xml;utf8;base64," + arr[i].icon + ");");
                $("div.menu-left:first").prepend(b);
//                console.log("!" + $('div.menu-left:first').html());
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Ошибка при получении кнопок для меню, textStatus: " + textStatus);
        }
    });
}

function logInAction() {
//    console.log("Это logInAction");
//    $("#loginDialog").dialog("open");
    var authDialog = new EJS({url: 'templates/authDialog.ejs'}).render();

    if (!w2ui.loginform) {
//        console.log("Создаём loginform");
        $().w2form({
            name: 'loginform',
            style: 'border: 0px; background-color: transparent;',
            formHTML: authDialog,
            fields: [
                { field: 'un', type: 'text', required: true },
                { field: 'pwd', type: 'password', required: true }
            ],
//            record: {
//                username    : 'John',
//                password     : 'Doe'
//            },
            actions: {
                "okAuth": function () {
                    var username = $('#un').val(), password = $('#pwd').val();
//                    console.log("username: " + username + ", password: " + password);
                    if(!username || !password) {
//                        console.log("Поля логина или пароля пустые");
                        $("#auth-result").html(constants.EMPTY_LOGIN_OR_PASSWORD);
                    } else if(username.indexOf(" ") !== -1 || password.indexOf(" ") !== -1) {
                        $("#auth-result").html(constants.LOGIN_OR_PW_CONTAINING_SPACE);
                    } else {
                        /* Проверить реквизиты. Если неправильно - выдать сообщение */
                        var authCode;
                        try {
                            authCode = btoa(username + ":" + password);
                        } catch(e) {
                            if("InvalidCharacterError" == e.name) {
                                $("#auth-result").html(constants.STRING_CONTAINS_INVALID_CHARACTER);
                            } else {
                                $("#auth-result").html(constants.UNRECOGNIZED_ERROR);
                            }
                        }
                        if(authCode) {
                            checkAuth(authCode, function(result) {
//                                console.log("result: " + JSON.stringify(result));
                                if(!result.success) {
                                    $("#auth-result").html(result.message);
                                } else {
//                                    console.log("Авторизация успешна");

                                    onAuthOK();

                                    /* Закрыть диалог */
                                    w2popup.close();
                                }
                            });
                        }

                    }
                },
                "cancelAuth": function () {
                    w2popup.close();
                }
            }
        });
    }

//    console.log("Открываем authform");
    $().w2popup('open', {
        title   : 'Авторизация',
        body    : '<div id="authform" style="width: 100%; height: 100%;"></div>',
        style   : 'padding: 15px 0px 0px 0px',
        width   : 350,
        height  : 200,
        speed: 0.1,
        showMax : false,
        onToggle: function (event) {
//            console.log("Это onToggle");
            $(w2ui.loginform.box).hide();
            event.onComplete = function () {
                $(w2ui.loginform.box).show();
                w2ui.loginform.resize();
            }
        },
        onOpen: function (event) {
            event.onComplete = function () {
                /* Рендерим */
                $('#w2ui-popup #authform').w2render('loginform');

                /* Очищаем поля */
                $("#un, #pwd").val("");

                /* Вешаем на поля логина и пароля слушатель,
                * чтобы гасить сообщение об ошибке при любых событиях в них */
                $("#un, #pwd").on("keydown", function() {
                    if($("#auth-result").html()) {
                        $("#auth-result").html("").removeAttr("title");
                    }
                });

                /* Вешаем слушатель на ENTER, чтобы по нажатию отправлять данные */
                $('#w2ui-popup #authform').keypress(function(e) {
                   if(e.keyCode === $.ui.keyCode.ENTER) {
                       /* Переносим фокус на кнопку ENTER */
                        $(this).find("button:eq(0)").focus();
                        $(this).find("button:eq(0)").trigger("click");
                    }
                });
            }
        }
    });
}

/* Акция для диалога регистрации */
function registerAction() {
//    console.log("Это registerAction");
    $.ajax({
        url: 'NewsCollector/profileForm',
        type: "POST",
        success: function (data, textStatus, jqXHR) {
            showRegisterForm(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Ошибка на сервере при получении формы профиля.");
        }
    });
}

function collectAndSendProfileData() {
    if (w2ui.profileForm) {
//        console.log("Это collectAndSendProfileData");
        var ret = {};
        ret.un = $("div.w2ui-field").find("input#un").val();
        ret.fn = $("div.w2ui-field").find("input#fn").val();
        ret.pn = $("div.w2ui-field").find("input#pn").val();

        /* Подписки */
        var checked = $("div.w2ui-field").find(".subs-cb-wrapper").find("input:checked");
        var subsArray = [];
        $.each(checked, function(index, value) {
            subsArray[index] = {"id":$(value).attr("id")};
        });
        ret.subs = subsArray;

        /* Пароль (вернее, его замена) - ставится, если поля пароля непустые */
        var password = $("div.w2ui-field").find("input#pw").val();
        var password1 = $("div.w2ui-field").find("input#pw1").val();
        if(password.length > 0 || password1.length > 0) {
            /* Проверяем и ставим */
            if(password != password1) {
                $("#profile-result").html("Пароль и его подтверждение не совпадают.");
                return;
            } else if(!/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{4,})/.test(password)) {
                $("#profile-result").html("Пароль должен содержать не менее 4 латинских букв разного регистра, цифр и спецсимволов.");
                return;
            } else {
                ret.pw = password;
            }
        }

//        console.log("ret: " + JSON.stringify(ret));

        $.ajax({
            url: 'NewsCollector/userProfile',
            type: "POST",
            data: {
                profileData: window.btoa(unescape(encodeURIComponent(JSON.stringify(ret)))),
            },
            success: function (data, textStatus, jqXHR) {
//                console.log("Это success, data: " + data);
                var dataJSON = JSON.parse(data);
                if(dataJSON.success) {
//                    console.log("Обновление профиля успешно.");
                    /* Закрыть форму */
                    w2popup.close();

                    onAuthOK();
                } else {
                    $("#profile-result").html(dataJSON.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Ошибка на сервере при обновлении профиля пользователя.");
                $("#profile-result").html("Ошибка на сервере.");
            }
        });
    }
}

function collectAndSendRegisterData() {
//    console.log("Это collectAndSendRegisterData");
    if (w2ui.registerForm) {
        var ret = {};
        var login = $("div.w2ui-field").find("input#un").val();
        if(!login) {
            $("#register-result").html("Не задан логин.");
            return;
        }

        if(!/^([a-zA-Z0-9_]{1,})$/.test(login)) {
            $("#register-result").html("Логин может состоять из латинских букв, цифр и знака подчерка.");
            return;
        }
        ret.un = login;

        ret.fn = $("div.w2ui-field").find("input#fn").val();

        ret.name = $("div.w2ui-field").find("input#name").val();

        var password = $("div.w2ui-field").find("input#pw").val();
        if(!password) {
            $("#register-result").html("Не задан пароль.");
            return;
        }

        var password1 = $("div.w2ui-field").find("input#pw1").val();
        if(password != password1) {
            $("#register-result").html("Пароль и его подтверждение не совпадают.");
            return;
        }

        if(!/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{4,})/.test(password)) {
            $("#register-result").html("Пароль должен содержать не менее 4 латинских букв разного регистра, цифр и спецсимволов.");
            return;
        }

        ret.pw = password;

        /* Подписки */
        var checked = $("div.w2ui-field").find(".subs-cb-wrapper").find("input:checked");
        var subsArray = [];
        $.each(checked, function(index, value) {
            subsArray[index] = {"id":$(value).attr("id")};
        });
        ret.subs = subsArray;

        $.ajax({
            url: 'NewsCollector/registerUser',
            type: "POST",
            data: {
                regData: window.btoa(unescape(encodeURIComponent(JSON.stringify(ret)))),
            },
            success: function (data, textStatus, jqXHR) {
                var dataJSON = JSON.parse(data);
//                console.log("dataJSON: " + JSON.stringify(dataJSON));
                if(dataJSON.success) {
                    console.log("Создание пользователя успешно.");
                    /* Закрыть форму */
                    w2popup.close();

                    onAuthOK();
                } else {
//                    console.log("Нет: " + dataJSON.message);
                    $("#register-result").html(dataJSON.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Ошибка на сервере при регистрации пользователя.");
                $("#register-result").html("Ошибка на сервере.");
            }
        });
    }
}

/* Показываем форму регистрации */
function showRegisterForm(html) {
//    console.log("Это showRegisterForm, html: " + html);
    if (!w2ui.registerForm) {
//        console.log("Создаём registerForm");
        $().w2form({
            name: 'registerForm',
            style: 'border: 0px; background-color: transparent;',
            formHTML: html,
            fields: [
                { field: 'un', type: 'text', required: true },
                { field: 'fn', type: 'text', required: false },
                { field: 'name', type: 'text', required: false },
                { field: 'pw', type: 'password', required: true },
                { field: 'pw1', type: 'password', required: true },
            ],
            actions: {
                "ok": function () {
                    $("#register-result").html("");
                    collectAndSendRegisterData();
                },
                "cancel": function () {
                    w2popup.close();
                }
            }
        });
    }

    $().w2popup('open', {
        title   : 'Регистрация',
        body    : '<div id="registerForm" style="width: 100%; height: 100%;"></div>',
        style   : 'padding: 0px 0px 0px 0px',
        width   : 450,
        height  : 500,
        speed: 0.1,
        showMax : false,
        onToggle: function (event) {
//            console.log("Это onToggle");
            $(w2ui.registerForm.box).hide();
            event.onComplete = function () {
                $(w2ui.registerForm.box).show();
                w2ui.registerForm.resize();
            }
        },
        onOpen: function (event) {
            event.onComplete = function () {
                /* Рендерим */
                $('#w2ui-popup #registerForm').w2render('registerForm');

                /* Очищаем поля */
                $("div.w2ui-field").find("input#un").val("")
                $("div.w2ui-field").find("input#fn").val("");
                $("div.w2ui-field").find("input#name").val("");
                $("div.w2ui-field").find("input#pw").val("");
                $("div.w2ui-field").find("input#pw1").val("");

                /* Вешаем слушатель на ENTER, чтобы по нажатию отправлять данные */
                $('#w2ui-popup #registerForm').keypress(function(e) {
                   if(e.keyCode === $.ui.keyCode.ENTER) {
                       /* Переносим фокус на кнопку ENTER */
                        $(this).find("button:eq(0)").focus();
                        $(this).find("button:eq(0)").trigger("click");
                    }
                });
            }
        }
    });
}

/* Акция для диалога выхода */
function logOutAction() {
//    console.log("Это logOutAction");
    var exitDialog = new EJS({url: 'templates/logoutDialog.ejs'}).render();

//    console.log("exitDialog: " + exitDialog);

    if (!w2ui.logoutform) {
//        console.log("Создаём logoutform");
        $().w2form({
            name: 'logoutform',
            style: 'border: 0px; background-color: transparent;',
            formHTML: exitDialog,
            actions: {
                "okExit": function () {
//                    console.log("Это Exit");

                    /* Отправить на сервер сигнал о том, что сессия завершена. */
                    exit(function(result) {
//                        console.log("Это cb выхода, result: " + JSON.stringify(result));
                        if(result.success) {
                            w2popup.close();

                            /* Удаляем сведения о пользователе */
                            window.userData = null;

                            /* Меняем кнопки в меню */
                            requestForMenuButtons();
                            /* Также убрать закладки */
                            /* Убираем все закладки, не помеченные классом permanent */
                            $("ul.tabs__caption > li:not(.permanent)").remove();
                            $("div.tabs > div.tabs__content:not(.permanent)").remove();

                            /* Переносим фокус на закладку "Последние" */
                            $("ul.tabs__caption > li").first().trigger("click");

                            /* Удаляем все привнесённые скрипты */
                            $("script.transient").remove();

                            /* Перезагружаем аккордеон */
                            requestForNewArticles();

                            /* Удаляем все следы былых поисков, буде такие остались. */
                            onClearButtonClick();
                        } else {
                            $("#logout-result").html(result.message);
                        }
                    });
                },
                "cancelExit": function () {
                    w2popup.close();
                }
            }
        });
    }

    $().w2popup('open', {
        title   : 'Выход',
        body    : '<div id="exitform" style="width: 100%; height: 100%;"></div>',
        style   : 'padding: 15px 0px 0px 0px',
        width   : 350,
        height  : 180,
        speed: 0,
        showMax : false,
        onToggle: function (event) {
//            console.log("Это onToggle");
            $(w2ui.logoutform.box).hide();
            event.onComplete = function () {
                $(w2ui.logoutform.box).show();
                w2ui.logoutform.resize();
            }
        },
        onOpen: function (event) {
            event.onComplete = function () {
                // specifying an onOpen handler instead is equivalent to specifying an onBeforeOpen handler, which would make this code execute too early and hence not deliver.
                $('#w2ui-popup #exitform').w2render('logoutform');

                /* Переносим фокус на кнопку OK */
                $("#okExit").focus();
            }
        }
    });
}

/* Акция для диалога профиля */
function profileAction() {
    /* Запросить с сервера форму диалога профиля */
    $.ajax({
        url: 'NewsCollector/profileForm',
        type: "POST",
        success: function (data, textStatus, jqXHR) {
//            console.log("Форма диалога профиля получена, data: " + data);
            showProfileForm(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Ошибка на сервере при получении формы профиля.");
        }
    });
}

/* Показываем форму профиля */
function showProfileForm(html) {
//    console.log("Это showProfileForm, html: " + html);
    if (!w2ui.profileForm) {
//    if (true) {
        $().w2form({
            name: 'profileForm',
            style: 'border: 0px; background-color: transparent;',
            formHTML: html,
            fields: [
                { field: 'un', type: 'text'},
                { field: 'fn', type: 'text'},
                { field: 'pn', type: 'text'},
                { field: 'pw', type: 'password'},
                { field: 'pw1', type: 'password'},
            ],
            actions: {
                "okProfile": function () {
                    /* Собрать и передать данные */
                    collectAndSendProfileData();
                },
                "cancelProfile": function () {
                    w2popup.close();
                }
            }
        });
    }

    $().w2popup('open', {
        title   : 'Профиль',
        body    : '<div id="profileform" style="width: 100%; height: 100%;"></div>',
        style   : 'padding: 15px 0px 0px 0px',
        width   : 450,
        height  : 540,
        speed: 0.1,
        showMax : false,
        onToggle: function (event) {
            $(w2ui.profileForm.box).hide();
            event.onComplete = function () {
                $(w2ui.profileForm.box).show();
                w2ui.profileForm.resize();
            }
        },
        onOpen: function (event) {
            event.onComplete = function () {
//                console.log("Это onOpen");
                /* Рендерим */
                $('#w2ui-popup #profileform').w2render('profileForm');

                /* Заполняем поля */
                w2ui.profileForm.record['un'] = window.userData.login;
                w2ui.profileForm.record['fn'] = window.userData.familyName;
                w2ui.profileForm.record['pn'] = window.userData.name;
                w2ui.profileForm.record['pw'] = "";
                w2ui.profileForm.record['pw1'] = "";
                /* Привилегии */
                $('#pList').html(window.userData.privileges);
                /* Доступы и подписки */
                $.each(window.userData.accesses, function( index, value ) {
                    var accDiv = $("<div/>")
                        .addClass("w2ui-field")
                        .attr("title", value.desc)
                        .append("<label>" + value.source + "</label>");
                    var wrapperDiv = $("<div/>")
                        .addClass("subs-cb-wrapper");
                    var inputEl = $("<input>")
                        .attr("type", "checkbox")
                        .attr("id", "subs_" + value.id)
                        .attr("name", "subs_" + value.id)
                        .attr("maxlength", "100")
                        .attr("style", "float: right;");
                    if(value.subs) {
                        inputEl.attr("checked", "checked");
                    }
                    wrapperDiv.append(inputEl);
                    accDiv.append(wrapperDiv);
                    $("fieldset.subs-fieldset").append(accDiv);
                });

                w2ui.profileForm.refresh();

                /* Вешаем слушатель на ENTER, чтобы по нажатию отправлять данные */
                $('#w2ui-popup #authform').keypress(function(e) {
                   if(e.keyCode === $.ui.keyCode.ENTER) {
                       /* Переносим фокус на кнопку ENTER */
                        $(this).find("button:eq(0)").focus();
                        $(this).find("button:eq(0)").trigger("click");
                    }
                });
            }
        }
    });
}

function onAuthOK() {
//    console.log("Это onAuthOK");

    /* Заполнить пользовательские данные */
    getUserData();

    /* Меняем кнопки в меню */
    requestForMenuButtons();

    /* Добавить/убрать закладки */
    requestForTabs();

    /* Заново запросить сервер о последних статьях */
    requestForNewArticles();

    /* Удалить следы поисков */
    onClearButtonClick();
}

/* Запрашиваем сервер о закладках для текущего пользователя (кроме "Последние" и "Поиск") */
function requestForTabs() {
//    console.log("Это requestForTabs");
    $.ajax({
        url: 'NewsCollector/getTabs',
        type: "POST",
        success: function (data, textStatus, jqXHR) {
//            console.log("Это requestForTabs success, data: " + data);
            var dataJSON = JSON.parse(data);
            renderTabs(dataJSON);
        },
        error: function (jqXHR, textStatus, errorThrown) {
//            cb({"success":false,"message":"Ошибка на сервере при получении закладок."});
            console.log("Ошибка на сервере при получении закладок.");
        }
    });
}

/* Отрисовываем полученные закладки */
function renderTabs(tabs) {
//    console.log("Это renderTabs, tabs: " + JSON.stringify(tabs));

    /* Удаляем скрипты */
    $("head").find("script.transient").remove();

    /* Удаляем привнесённые закладки */
    $("ul.tabs__caption").find("li:not('.permanent')").remove();

    /* Удаляем контент привнесённых закладок */
    $("div.tabs").find("div.tabs__content:not('.permanent')").remove();

    for(var i= 0; i < tabs.length; i++) {
        $(".tabs__caption").append("<li>" + tabs[i].name + "</li>");

        $(".tabs").append("<div class='tabs__content'>" + tabs[i].content + "</div>")

        $("head").append("<script src='js/" + tabs[i].script + "' type='text/javascript' class='transient'></script>");

        var fn = window[tabs[i].func];
        if(fn && typeof fn === 'function') {
            fn();
        }
    }
}

function exit(cb) {
//    console.log("Это exit, cb: " + cb);
    $.ajax({
        url: 'NewsCollector/logout',
        type: "POST",
        success: function (data, textStatus, jqXHR) {
//            console.log("Это exit success, data: " + data);
            var dataJSON = JSON.parse(data);
            cb(dataJSON);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            cb({"success":false,"message":"Ошибка на сервере при выходе из системы."});
        }
    });
}

function checkAuth(authCode, cb) {
//    console.log("Это checkAuth, authCode: " + authCode);

    $.ajax({
        url: 'NewsCollector/getAuth',
        type: "POST",
        data: {
            authCode: authCode,
        },
        success: function (data, textStatus, jqXHR) {
            var dataJSON = JSON.parse(data);
            cb(dataJSON);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            cb({"success":false,"message":"Ошибка при авторизации на сервере"});
        }
    });
}