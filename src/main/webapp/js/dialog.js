showErrorDialog = function(text) {
    $("<div>" + text + "</div>").dialog({
        title: "Ошибка",
        closeOnEscape: true,
        buttons: [
            {
                text: "OK",
                click: function() {
                    $( this ).dialog( "close" );
                }
            }
        ]
    });
};