$(document).ready(function() {
//    console.log( "ready!" );

    var sourcesJSON;
    var sourcesSelectHTML;

    $("#loginDialog").dialog({
        autoOpen: false
    });

    /* Запрашиваем с сервера кнопки меню - в зависимости от сессии. */
    requestForMenuButtons();

    /* Добавить/убрать закладки */
    requestForTabs();

    /* Запрашиваем сервер о последних статьях */
    requestForNewArticles(true);
    
    /* Запрашиваем сервер об имеющихся источниках */
    requestForSources();
    
    /* Открываем веб-сокет */
    var wsUri = "ws://" + document.location.host + document.location.pathname + "endpoint";
    startWS(wsUri);

    /* Запрашиваем с сервера данные текущего пользователя */
    getUserData();

    /* Вешаем слушатель на кнопку очистки */
    $("#clearAllButton").on("click", function() {
        onClearButtonClick();
    });

    /* Вешаем слушатель на кнопку поиска */
    $("#submitSearchButton").on("click", function() {
        submitSearchForm();
    });

    $("#download-select").on("change", function() {
        downloadFoundArticles($(this).val());
        $("#download-select option").eq(0).prop("selected", true);
    });

    /* Вешаем слушатель на мышь с целью отслеживать выход за край документа */
    $(".bar-left").on("mousemove", function (evt) {
        if(evt.clientX <= 0) {
            $(".menu-left").animate({left: "0"}, "fast");
        }
    });
    /* Вешаем слушатель на меню - будем его закрывать при выводе мыши */
    $(".menu-left").mouseleave(function (evt) {
        $(".menu-left").animate({left: "-48px"}, "fast");
    });

    $.datepicker.setDefaults({
        closeText: "Закрыть",
        prevText: "&#x3C;Пред",
        nextText: "След&#x3E;",
        currentText: "Сегодня",
        monthNames: [ "Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь" ],
        monthNamesShort: [ "Янв","Фев","Мар","Апр","Май","Июн","Июл","Авг","Сен","Окт","Ноя","Дек" ],
        dayNames: [ "воскресенье","понедельник","вторник","среда","четверг","пятница","суббота" ],
        dayNamesShort: [ "вск","пнд","втр","срд","чтв","птн","сбт" ],
        dayNamesMin: [ "Вс","Пн","Вт","Ср","Чт","Пт","Сб" ],
        weekHeader: "Нед",
        dateFormat: "dd MM yy",
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ""
    });

    /* Инициализируем поиск */
    initSearching();
});

Number.prototype.pad = function(size) {
    var s = String(this);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
}

function requestForNewArticles(initAccordion) {
    /* Сначала очистим */
    $("#accordion").html("");

    /* Влепить крутилку */
    $("#accordion_waiter").addClass("busy_indicator");

    $.ajax({
        url: 'NewsCollector/getArticles',
        type: "POST",
        data: {
            articlesCount: 50
        },
        success: function (data, textStatus, jqXHR) {
//            console.log("data: " + data);
            var dataJSON = JSON.parse(data);

            var articlesHTML = new EJS({url: 'templates/articles.ejs'}).render({
                data: dataJSON.articles
            });
//            console.log("articlesHTML: " + articlesHTML);
            $("#accordion").html(articlesHTML);
            if(initAccordion) {
                $("#accordion").accordion({
                    active:false,
                    collapsible: true,
                    heightStyle: "content"
                });
            } else {
                $("#accordion").accordion("refresh");
            }

            $("#accordion_waiter").removeClass("busy_indicator");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Ошибка при получении последних статей, textStatus: " + textStatus);
        }
    });
}

function startWS(wsServerLocation){
    ws = new WebSocket(wsServerLocation);
    ws.onmessage = function(evt) {
        try {
            var dataObj = JSON.parse(evt.data);
            if(dataObj.newArticle) {
                var dataArr = [];
                dataArr.push(dataObj.newArticle);
                var articlesHTML = new EJS({url: 'templates/articles.ejs'}).render({
                    data: dataArr,
                });
                $("#accordion").prepend(articlesHTML);
                /* Также удалить последний элемент */
                $("#accordion").children("h3").last().remove();
                $("#accordion").children("div").last().remove();
                $("#accordion").accordion("refresh");
            } else if(dataObj.logMessage) {
                addLogMessage(dataObj.logMessage);
            } else if(dataObj.notice && dataObj.notice == 'sourcesUpdated') {
//                console.log("Получено уведомление sourcesUpdated.");
                /* Перестроить комбобоксы на закладках поиска и источников. */
                requestForSources();
            }
        } catch (e) {
            console.log("Не удалось распарсить JSON, пришедший по веб-сокету: " + e);
        }
    };
    ws.onclose = function(){
//        console.log("Отключились");
        // Try to reconnect
        setTimeout(function(){
            startWS(wsServerLocation)
        }, 10000);
    };
    ws.onerror = function(evt) {
//        console.log("Ошибка веб-сокета: " + evt.data);
    };
    ws.onopen = function(evt) {
        console.log("Веб-сокет открыт");
    };
}

/* Запрашиваем с сервера данные текущего пользователя */
function getUserData() {
    $.ajax({
        url: 'NewsCollector/getUserData',
        type: "GET",
        success: function (data, textStatus, jqXHR) {
//            console.log("Получены данные пользователя: " + data);
            window.userData = JSON.parse(data).user;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Ошибка при получении данных текущего пользователя, textStatus: " + textStatus);
        }
    });
}

/* Запрашиваем сервер об имеющихся источниках */
function requestForSources() {
//    console.log("Это requestForSources");
    $.ajax({
        url: 'NewsCollector/getSources',
        type: "GET",
        success: function (data, textStatus, jqXHR) {
            sourcesJSON = JSON.parse(data);
            fillSearchSelect();
            if(typeof fillSourcesSelect === 'function') {
                fillSourcesSelect();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Ошибка при получении списка источников, textStatus: " + textStatus);
        }
    });
}